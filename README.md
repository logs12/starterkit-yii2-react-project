INSTALLATION PROGECT PROPER-TEAM-MANAGEMENT
-------------------

1.Cloning repository develop branch

```
$ git clone
```

2.Go to the server part directory project
```
cd api/
```
3.Install server dependencies
```
composer update
```

4.Running migrations
```
php yii migrate
```

5.Go to the frontend part directory project
```
cd ../app
```
6.Install frontend dependencies
```
npm i
```
7.Running the Build Process frontend
```
npm deploy
```
In the branch the master is the same as in the developer
import BaseModelReducer from '../../../common/reducers/BaseModelReducer';

import {
    ROOM_MODEL_INIT,
    ROOM_MODEL_UPDATE,
    ROOM_FORM_MODEL_ERROR,
} from '../../../common/constants/index';

let initialState = {};
export default function RoomModelReducer(state = initialState, action) {

    switch (action.type) {

        case ROOM_MODEL_INIT: {
            return BaseModelReducer.initModel(state, action);
        }

        case ROOM_MODEL_UPDATE: {
            return BaseModelReducer.updateModel(state, action);
        }

        case ROOM_FORM_MODEL_ERROR: {
            return BaseModelReducer.errorModel(state, action);
        }
    }

    return state;
}
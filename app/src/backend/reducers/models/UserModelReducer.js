import BaseModelReducer from '../../../common/reducers/BaseModelReducer';

import {
    USER_MODEL_INIT,
    USER_MODEL_UPDATE,
    USER_FORM_MODEL_ERROR,
} from '../../../common/constants/index';

let initialState = {};

export default function UserModelReducer(state = initialState, action) {

    switch (action.type) {

        case USER_MODEL_INIT: {
            return BaseModelReducer.initModel(state, action);
        }

        case USER_MODEL_UPDATE: {
            return BaseModelReducer.updateModel(state, action);
        }

        case USER_FORM_MODEL_ERROR: {
            return BaseModelReducer.errorModel(state, action);
        }
    }

    return state;
}
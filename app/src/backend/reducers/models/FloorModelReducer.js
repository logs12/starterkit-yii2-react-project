import BaseModelReducer from '../../../common/reducers/BaseModelReducer';

import {
    FLOOR_MODEL_INIT,
    FLOOR_MODEL_UPDATE,
    FLOOR_FORM_MODEL_ERROR,
    FLOOR_FILE_INPUT_MULTIPLE_WIDGET_CONVERT_TO_BASE64,
    FLOOR_FILE_INPUT_MULTIPLE_WIDGET_UPDATE_VALUE,
    FLOOR_FILE_INPUT_MULTIPLE_WIDGET_DELETE_VALUE,
} from '../../../common/constants/index';

let initialState = {};


export default function FloorModelReducer(state = initialState, action) {

    switch (action.type) {

        case FLOOR_MODEL_INIT: {
            return BaseModelReducer.initModel(state, action);
        }

        case FLOOR_MODEL_UPDATE: {
            return BaseModelReducer.updateModel(state, action);
        }

        case FLOOR_FORM_MODEL_ERROR: {
            return BaseModelReducer.errorModel(state, action);
        }

        case FLOOR_FILE_INPUT_MULTIPLE_WIDGET_CONVERT_TO_BASE64: {
            return BaseModelReducer.fileInputMultipleWidgetConvertToBase64(state, action);
        }

        case FLOOR_FILE_INPUT_MULTIPLE_WIDGET_UPDATE_VALUE: {
            return BaseModelReducer.fileInputMultipleWidgetUpdateValue(state, action);
        }

        case FLOOR_FILE_INPUT_MULTIPLE_WIDGET_DELETE_VALUE: {
            return BaseModelReducer.fileInputMultipleWidgetDeleteValue(state, action);
        }
    }

    return state;
}
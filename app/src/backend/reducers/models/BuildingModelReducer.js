import BaseModelReducer from '../../../common/reducers/BaseModelReducer';

import {
    BUILDING_MODEL_INIT,
    BUILDING_MODEL_UPDATE,
    BUILDING_FORM_MODEL_ERROR,
} from '../../../common/constants/index';

let initialState = {};

export default function BuildingModelReducer(state = initialState, action) {
    
    switch (action.type) {

        case BUILDING_MODEL_INIT: {
            return BaseModelReducer.initModel(state, action);
        }

        case BUILDING_MODEL_UPDATE: {
            return BaseModelReducer.updateModel(state, action);
        }
            
        case BUILDING_FORM_MODEL_ERROR: {
            return BaseModelReducer.errorModel(state, action);
        }
    }
    
    return state;
}
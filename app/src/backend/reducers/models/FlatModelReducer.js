import BaseModelReducer from '../../../common/reducers/BaseModelReducer';

import {
    FLAT_MODEL_INIT,
    FLAT_MODEL_UPDATE,
    FLAT_FORM_MODEL_ERROR,
    FLAT_FILE_INPUT_MULTIPLE_WIDGET_CONVERT_TO_BASE64,
    FLAT_FILE_INPUT_MULTIPLE_WIDGET_UPDATE_VALUE,
    FLAT_FILE_INPUT_MULTIPLE_WIDGET_DELETE_VALUE,
} from '../../../common/constants/index';

let initialState = {};

export default function FlatModelReducer(state = initialState, action) {

    switch (action.type) {

        case FLAT_MODEL_INIT: {
            return BaseModelReducer.initModel(state, action);
        }

        case FLAT_MODEL_UPDATE: {
            return BaseModelReducer.updateModel(state, action);
        }

        case FLAT_FORM_MODEL_ERROR: {
            return BaseModelReducer.errorModel(state, action);
        }

        case FLAT_FILE_INPUT_MULTIPLE_WIDGET_CONVERT_TO_BASE64: {
            return BaseModelReducer.fileInputMultipleWidgetConvertToBase64(state, action);
        }

        case FLAT_FILE_INPUT_MULTIPLE_WIDGET_UPDATE_VALUE: {
            return BaseModelReducer.fileInputMultipleWidgetUpdateValue(state, action);
        }

        case FLAT_FILE_INPUT_MULTIPLE_WIDGET_DELETE_VALUE: {
            return BaseModelReducer.fileInputMultipleWidgetDeleteValue(state, action);
        }
    }

    return state;
}
import * as _ from 'lodash';
import {
    FLOORS_GET,
    FLOOR_DELETE,
} from '../../../common/constants/index';

export default function FloorCollectionReducer(state = [], action) {

    switch (action.type) {
        case FLOORS_GET: {
            if (!_.isArray(action.payload)) {
                return [
                    {...action.payload}
                ];
            }
            return [
                ...action.payload
            ];
        }
        case FLOOR_DELETE: {
            // Filter the collection of deleted item
            return [
                ..._.filter(state, (user) => user.id !== action.payload['flatId']),
            ];
        }
        default: {
            return state
        }
    }
}

import * as _ from 'lodash';
import {
    BUILDINGS_GET,
    BUILDING_DELETE,
} from '../../../common/constants/index';

export default function BuildingCollectionReducer(state = [], action) {

    switch (action.type) {
        case BUILDINGS_GET: {
            if (!_.isArray(action.payload)) {
                return [
                    {...action.payload}
                ];
            }
            return [
                ...action.payload
            ];
        }
        case BUILDING_DELETE: {
            // Filter the collection of deleted item
            return [
                ..._.filter(state, (user) => user.id !== action.payload['buildingId']),
            ];
        }
        default: {
            return state
        }
    }
}

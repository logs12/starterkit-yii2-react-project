import * as _ from 'lodash';
import {
    FLATS_GET,
    FLAT_DELETE,
} from '../../../common/constants/index';

export default function FlatCollectionReducer(state = [], action) {

    switch (action.type) {
        case FLATS_GET: {
            if (!_.isArray(action.payload)) {
                return [
                    {...action.payload}
                ];
            }
            return [
                ...action.payload
            ];
        }
        case FLAT_DELETE: {
            // Filter the collection of deleted item
            return [
                ..._.filter(state, (user) => user.id !== action.payload['flatId']),
            ];
        }
        default: {
            return state
        }
    }
}

import * as _ from 'lodash';
import {
    ROOMS_GET,
    ROOM_DELETE,
} from '../../../common/constants/index';

export default function RoomCollectionReducer(state = [], action) {

    switch (action.type) {
        case ROOMS_GET: {
            if (!_.isArray(action.payload)) {
                return [
                    {...action.payload}
                ];
            }
            return [
                ...action.payload
            ];
        }
        case ROOM_DELETE: {
            // Filter the collection of deleted item
            return [
                ..._.filter(state, (user) => user.id !== action.payload['roomId']),
            ];
        }
        default: {
            return state
        }
    }
}

import React, { Component } from 'react';
import Article from '../../../common/widgets/article-widget/component';


export default class DashboardPage extends Component{

    render()  {

        return (
            <div className="dashboard-page" >
                <Article><h2>Proper Team Management</h2></Article>
            </div>
        )
    }
};
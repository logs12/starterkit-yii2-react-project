import { BaseFetch } from '../../common/services/BaseFetch';

import {
    USER_URL_REQUEST,
    USERS_GET,
    USER_DELETE,
    USER_FORM_WIDGET_REQUEST,
    USER_FORM_WIDGET_SUCCESS,
    USER_FORM_MODEL_ERROR,
    PAGINATION_GET,
} from '../../common/constants';

export function usersGetAction(options) {

    return (dispatch, getState) => {

        const userId = options && options.hasOwnProperty('userId') ? options.userId : null;
        const condition = options && options.hasOwnProperty('condition') ? encodeURI(options.condition) : '';

        return BaseFetch.get({
            url: `${USER_URL_REQUEST(userId)}${condition}`,
            dispatch: dispatch,
            success: object => {
                dispatch({
                    type: USERS_GET,
                    payload: object,
                });
            },
        });
    };
}

export function userCreateAction(data, options) {
    return {
        types: [
            USER_FORM_WIDGET_REQUEST,
            USER_FORM_WIDGET_SUCCESS,
            USER_FORM_MODEL_ERROR,
        ],
        promise: (dispatch, getState) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'POST' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}


export function userUpdateAction(data, options) {
    return {
        types: [
            USER_FORM_WIDGET_REQUEST,
            USER_FORM_WIDGET_SUCCESS,
            USER_FORM_MODEL_ERROR,
        ],
        promise: (dispatch, getState) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'PUT' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}

export function userDeleteAction(userId) {
    return (dispatch, getState) => {
        return BaseFetch.delete(
            {
                url: USER_URL_REQUEST(userId),
                dispatch: dispatch,
                getState: getState,
                success: () => {
                    dispatch({
                        type: USER_DELETE,
                        payload: {
                            userId: userId,
                        }
                    });
                },
            },
            {
                messageSnackbar: 'User deleted?',
                actionOneTitle: 'Yes',
                actionTwoTitle: 'No',
            },
        );
    }
}
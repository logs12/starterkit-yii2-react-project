import { BaseFetch } from '../../common/services/BaseFetch';

import {
    BUILDING_URL_REQUEST,
    BUILDINGS_GET,
    BUILDING_DELETE,
    BUILDING_FORM_WIDGET_REQUEST,
    BUILDING_FORM_WIDGET_SUCCESS,
    BUILDING_FORM_MODEL_ERROR,
    PAGINATION_GET,
} from '../../common/constants';

const getPaginationData = (response) => {
    return {
        total: response.headers.get('X-Pagination-Total-Count'),
        current: response.headers.get('X-Pagination-Current-Page'),
        perPage: response.headers.get('X-Pagination-Per-Page'),
    };
};

export function buildingsGetAction(options) {

    return (dispatch, getState) => {

        const userId = options && options.hasOwnProperty('userId') ? options.userId : null;
        const condition = options && options.hasOwnProperty('condition') ? options.condition : '';
        const onSuccess = options && options.hasOwnProperty('onSuccess') ? options.onSuccess : null;

        return BaseFetch.get({
            url: `${BUILDING_URL_REQUEST(userId)}${condition}`,
            dispatch: dispatch,
            success: object => {
                dispatch({
                    type: BUILDINGS_GET,
                    payload: object,
                });
                if (typeof onSuccess == 'function') {
                    onSuccess();
                }
            },
        });
    };
}

export function buildingCreateAction(data, options) {
    return {
        types: [
            BUILDING_FORM_WIDGET_REQUEST,
            BUILDING_FORM_WIDGET_SUCCESS,
            BUILDING_FORM_MODEL_ERROR,
        ],
        promise: (dispatch, getState) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'POST' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}


export function buildingUpdateAction(data, options) {
    return {
        types: [
            BUILDING_FORM_WIDGET_REQUEST,
            BUILDING_FORM_WIDGET_SUCCESS,
            BUILDING_FORM_MODEL_ERROR,
        ],
        promise: (dispatch, getState) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'PUT' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}

export function buildingDeleteAction(buildingId) {
    return (dispatch, getState) => {
        return BaseFetch.delete(
            {
                url: BUILDING_URL_REQUEST(buildingId),
                dispatch: dispatch,
                getState: getState,
                success: () => {
                    dispatch({
                        type: BUILDING_DELETE,
                        payload: {
                            buildingId: buildingId,
                        }
                    });
                },
            },
            {
                messageSnackbar: 'Building deleted?',
                actionOneTitle: 'Yes',
                actionTwoTitle: 'No',
            }
        );
    }
}
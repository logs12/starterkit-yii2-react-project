import { BaseFetch } from '../../common/services/BaseFetch';

import {
    FLAT_URL_REQUEST,
    FLATS_GET,
    FLAT_DELETE,
    FLAT_FORM_WIDGET_REQUEST,
    FLAT_FORM_WIDGET_SUCCESS,
    FLAT_FORM_MODEL_ERROR,
    PAGINATION_GET,
} from '../../common/constants';

const getPaginationData = (response) => {
    return {
        total: response.headers.get('X-Pagination-Total-Count'),
        current: response.headers.get('X-Pagination-Current-Page'),
        perPage: response.headers.get('X-Pagination-Per-Page'),
    };
};

export function flatsGetAction(options) {

    return (dispatch, getState) => {

        const userId = options && options.hasOwnProperty('userId') ? options.userId : null;
        const condition = options && options.hasOwnProperty('condition') ? encodeURI(options.condition) : '';

        return BaseFetch.get({
            url: `${FLAT_URL_REQUEST(userId)}${condition}`,
            dispatch: dispatch,
            success: object => {
                dispatch({
                    type: FLATS_GET,
                    payload: object,
                });
            },
        });
    };
}

export function flatCreateAction(data, options) {
    return {
        types: [
            FLAT_FORM_WIDGET_REQUEST,
            FLAT_FORM_WIDGET_SUCCESS,
            FLAT_FORM_MODEL_ERROR,
        ],
        promise: (dispatch, getState) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'POST' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}


export function flatUpdateAction(data, options) {
    return {
        types: [
            FLAT_FORM_WIDGET_REQUEST,
            FLAT_FORM_WIDGET_SUCCESS,
            FLAT_FORM_MODEL_ERROR,
        ],
        promise: (dispatch, getState) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'PUT' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}

export function flatDeleteAction(flatId) {
    return (dispatch, getState) => {
        return BaseFetch.delete(
            {
                url: FLAT_URL_REQUEST(flatId),
                dispatch: dispatch,
                getState: getState,
                success: () => {
                    dispatch({
                        type: FLAT_DELETE,
                        payload: {
                            flatId: flatId,
                        }
                    });
                },
            },
            {
                messageSnackbar: 'Flat deleted?',
                actionOneTitle: 'Yes',
                actionTwoTitle: 'No',
            }
        );
    }
}
import { BaseFetch } from '../../common/services/BaseFetch';

import {
    ROOM_URL_REQUEST,
    ROOMS_GET,
    ROOM_DELETE,
    ROOM_FORM_WIDGET_REQUEST,
    ROOM_FORM_WIDGET_SUCCESS,
    ROOM_FORM_MODEL_ERROR,
    PAGINATION_GET,
} from '../../common/constants';

const getPaginationData = (response) => {
    return {
        total: response.headers.get('X-Pagination-Total-Count'),
        current: response.headers.get('X-Pagination-Current-Page'),
        perPage: response.headers.get('X-Pagination-Per-Page'),
    };
};

export function roomsGetAction(options) {

    return (dispatch, getState) => {

        const userId = options && options.hasOwnProperty('userId') ? options.userId : null;
        const condition = options && options.hasOwnProperty('condition') ? encodeURI(options.condition) : '';

        return BaseFetch.get({
            url: `${ROOM_URL_REQUEST(userId)}${condition}`,
            dispatch: dispatch,
            success: object => {
                dispatch({
                    type: ROOMS_GET,
                    payload: object,
                });
            },
        });
    };
}

export function roomCreateAction(data, options) {
    return {
        types: [
            ROOM_FORM_WIDGET_REQUEST,
            ROOM_FORM_WIDGET_SUCCESS,
            ROOM_FORM_MODEL_ERROR,
        ],
        promise: (dispatch, getState) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'POST' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}


export function roomUpdateAction(data, options) {
    return {
        types: [
            ROOM_FORM_WIDGET_REQUEST,
            ROOM_FORM_WIDGET_SUCCESS,
            ROOM_FORM_MODEL_ERROR,
        ],
        promise: (dispatch, getState) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'PUT' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}

export function roomDeleteAction(roomId) {
    return (dispatch, getState) => {
        return BaseFetch.delete(
            {
                url: ROOM_URL_REQUEST(roomId),
                dispatch: dispatch,
                getState: getState,
                success: () => {
                    dispatch({
                        type: ROOM_DELETE,
                        payload: {
                            roomId: roomId,
                        }
                    });
                },
            },
            {
                messageSnackbar: 'Room deleted?',
                actionOneTitle: 'Yes',
                actionTwoTitle: 'No',
            }
        );
    }
}
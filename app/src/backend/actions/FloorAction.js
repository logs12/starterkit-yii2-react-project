import { BaseFetch } from '../../common/services/BaseFetch';

import {
    FLOOR_URL_REQUEST,
    FLOORS_GET,
    FLOOR_DELETE,
    FLOOR_FORM_WIDGET_REQUEST,
    FLOOR_FORM_WIDGET_SUCCESS,
    FLOOR_FORM_MODEL_ERROR,
    PAGINATION_GET,
} from '../../common/constants';

const getPaginationData = (response) => {
    return {
        total: response.headers.get('X-Pagination-Total-Count'),
        current: response.headers.get('X-Pagination-Current-Page'),
        perPage: response.headers.get('X-Pagination-Per-Page'),
    };
};

export function floorsGetAction(options) {

    return (dispatch, getState) => {

        const userId = options && options.hasOwnProperty('userId') ? options.userId : null;
        const condition = options && options.hasOwnProperty('condition') ? encodeURI(options.condition) : '';
        const onSuccess = options && options.hasOwnProperty('onSuccess') ? options.onSuccess : null;

        return BaseFetch.get({
            url: `${FLOOR_URL_REQUEST(userId)}${condition}`,
            dispatch: dispatch,
            success: object => {
                dispatch({
                    type: FLOORS_GET,
                    payload: object,
                });
                if (typeof onSuccess == 'function') {
                    onSuccess();
                }
            },
        });
    };
}

export function floorCreateAction(data, options) {
    return {
        types: [
            FLOOR_FORM_WIDGET_REQUEST,
            FLOOR_FORM_WIDGET_SUCCESS,
            FLOOR_FORM_MODEL_ERROR,
        ],
        promise: (dispatch, getState) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'POST' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}


export function floorUpdateAction(data, options) {
    return {
        types: [
            FLOOR_FORM_WIDGET_REQUEST,
            FLOOR_FORM_WIDGET_SUCCESS,
            FLOOR_FORM_MODEL_ERROR,
        ],
        promise: (dispatch, getState) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'PUT' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}

export function floorDeleteAction(floorId) {
    return (dispatch, getState) => {
        return BaseFetch.delete(
            {
                url: FLOOR_URL_REQUEST(floorId),
                dispatch: dispatch,
                getState: getState,
                success: () => {
                    dispatch({
                        type: FLOOR_DELETE,
                        payload: {
                            floorId: floorId,
                        }
                    });
                },
            },
            {
                messageSnackbar: 'Floor deleted?',
                actionOneTitle: 'Yes',
                actionTwoTitle: 'No',
            }
        );
    }
}
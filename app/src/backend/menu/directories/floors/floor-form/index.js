import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import _ from 'lodash';
import Form from '../../../../../common/widgets/form-widget';
import TextInputWidget from '../../../../../common/widgets/text-input-widget';
import ButtonLoadingWidget from '../../../../../common/widgets/button-loading-widget';
import FileInputMultipleWidget from '../../../../../common/widgets/file-input-multiple-widget';
import BackToListButton from '../../../../components/BackToListButton';
import Select2Widget from '../../../../../common/widgets/select2-widget';


import { floorsGetAction } from '../../../../actions/FloorAction';
import {
    FLOOR_URL_REQUEST,
    FLOORS_ROUTE,
    FLOOR_CREATE_ROUTE,
} from "../../../../../common/constants";

const mapToStateProps = (state, ownProps) => ({
    floorId: Number(ownProps.params.id),
    floors: state.Floor.collection,
    floor: _.find(state.Floor.collection, {id: Number(ownProps.params.id)}),
});

const mapDispatchToProps = (dispatch) => ({
    floorsGetAction: bindActionCreators(floorsGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapToStateProps, mapDispatchToProps)

export default class FloorsForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'floorCreateAction';

    componentWillMount() {
        // If entity update
        if (this.props.floorId) {
            if (!this.props.floors.length) {
                this.props.floorsGetAction(this.props.floorId);
            }
            this.formAction = 'floorUpdateAction';
        }
    }

    render () {

        const { floor, floorId, pushToRouter } = this.props;

        return (
            <div className="floor-page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={FLOORS_ROUTE} />
                    <Form
                        actionName={this.formAction}
                        entityName="Floor"
                        initModel={floor}
                        attributes={[
                            'number',
                            'title',
                            'building_id',
                            'building_address',
                            'photos'
                        ]}
                        url={FLOOR_URL_REQUEST(floorId)}>
                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'number'
                                    label = 'Number floor'
                                    initFocused
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'title'
                                    label = 'Title'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <Select2Widget
                                    entityName="Floor"
                                    name="building_id"
                                    nameHidden="building_address"
                                    label="Building"
                                    relationGetActionName="buildingsGetAction"
                                    relationCollectionName="Building"
                                    relationFieldId="id"
                                    relationFieldName="address_full"
                                    relationFieldsToSearch={['address_one', 'address_two']}
                                />
                            </div>
                        </div>

                        <div>
                            <FileInputMultipleWidget
                                name = 'photos'
                                type = 'file'
                                title = 'Select file to upload'
                            />
                        </div>

                        <div className="form-widget__actions mdl-card__actions mdl-card--border mdl-grid">
                            <ButtonLoadingWidget
                                label="Save"
                                type="saveAndBackToList"
                                backToListUrl={FLOORS_ROUTE}
                            />
                            { !floorId ?
                                <ButtonLoadingWidget
                                    label="Save & New"
                                    type="saveAndMore"
                                    backToListUrl={FLOOR_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}
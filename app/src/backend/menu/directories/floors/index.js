import React from 'react';
import TableWidget from '../../../../common/widgets/table-widget';

import {
    FLOORS_ROUTE,
    FLOOR_CREATE_ROUTE,
    FLOOR_VIEW_ROUTE,
    FLOOR_UPDATE_ROUTE,
} from '../../../../common/constants';

const FloorsPage = () => {
    return (
        <div className="floors-page">
            <TableWidget
                actionsTableHeader={[
                    {
                        title: 'Add',
                        iconName: 'add',
                        link: FLOOR_CREATE_ROUTE,
                    }
                ]}

                rowMenuActions = {{
                    actionView: {
                        title: 'View',
                        url: FLOOR_VIEW_ROUTE,
                        settings: {
                            clickRow: true,
                        },
                    },
                    actionUpdate: {
                        title: 'Edit',
                        url: FLOOR_UPDATE_ROUTE,
                        settings: {
                            nameIconButton: 'edit',
                        },
                    },
                    actionDelete: {
                        title: 'Delete',
                        dispatchAction: 'floorDeleteAction',
                        settings: {
                            nameIconButton: 'delete',
                        },
                    },
                }}

                attributes={{
                    number: {
                        title: 'Number floor',
                        propsTableHeader: {
                            tooltip: 'Number floor',
                        },
                    },
                    title: {
                        title: 'Title',
                        propsTableHeader: {
                            tooltip: 'Title',
                        },
                    },

                    number_floor: {
                        title: 'Number floor',
                        propsTableHeader: {
                            tooltip: 'Number floor',
                        },
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "Floor",
                        url: FLOORS_ROUTE,
                        searchFields: [
                            'number',
                        ]
                    },
                    pagination: {
                        entityName: "Floor",
                        url: FLOORS_ROUTE,
                    }
                }}

                collectionName="Floor"
                actionName="floorsGetAction"
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default FloorsPage;
import './style.scss';

import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from "redux";
import * as actions from '../../../../actions/FloorAction';
import Article from '../../../../../common/widgets/article-widget/component';
import BackToListButton from '../../../../components/BackToListButton';
import GalleryLightboxWidget from '../../../../../common/widgets/gallery-lightbox-widget';
//import CategoryToContentWidget from '../../../../../common/widgets/category-to-content-widget';

import {
    FLOORS_ROUTE,
    ROOM_VIEW_ROUTE
} from "../../../../../common/constants";

const mapStateToProps = (state, ownProps) => ({
    floors: state.Floor.collection,
    floor: _.find(state.Floor.collection, {id: Number(ownProps.params.id)}),
    currentRoute: state.routing.locationBeforeTransitions.pathname,
});

const mapDispatchToProps = (dispatch) => ({
    floorActions: bindActionCreators(actions, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class FloorView extends Component {

    componentWillMount() {
        if (_.isEmpty(this.props.floor)) {
            this.props.floorActions.floorsGetAction();
        }
    }

    render() {

        const { floor, pushToRouter, currentRoute } = this.props;

        return (
            <div className="flat-view-page">
                <Article>

                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={FLOORS_ROUTE} />

                    {(!_.isEmpty(floor)) ?
                        <div>
                            <p><b>Number:</b> {floor.number}</p>
                            <p><b>title:</b> {floor.title}</p>

                            <GalleryLightboxWidget images={floor.photos} />

                            {/*<CategoryToContentWidget
                                categoryCollection={floor.flats}
                                collectionContentName="Rooms"
                                actionName="roomsGetAction"
                                conditionFieldName="flat_id"
                                urlContentView={ROOM_VIEW_ROUTE}
                                currentRoute={currentRoute}
                                categoryTitle="Flats"
                                contentTitle="Rooms"
                            />*/}
                        </div>
                    : null}

                </Article>
            </div>
        )
    }
}
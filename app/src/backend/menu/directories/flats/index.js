import React from 'react';
import TableWidget from '../../../../common/widgets/table-widget';

import {
    FLATS_ROUTE,
    FLAT_CREATE_ROUTE,
    FLAT_VIEW_ROUTE,
    FLAT_UPDATE_ROUTE,
} from '../../../../common/constants';

const FlatsPage = () => {
    return (
        <div className="flats-page">
            <TableWidget
                actionsTableHeader={[
                    {
                        title: 'Add',
                        iconName: 'add',
                        link: FLAT_CREATE_ROUTE,
                    }
                ]}

                rowMenuActions = {{
                    actionView: {
                        title: 'View',
                        url: FLAT_VIEW_ROUTE,
                        settings: {
                            clickRow: true,
                        },
                    },
                    actionUpdate: {
                        title: 'Edit',
                        url: FLAT_UPDATE_ROUTE,
                        settings: {
                            nameIconButton: 'edit',
                        },
                    },
                    actionDelete: {
                        title: 'Delete',
                        dispatchAction: 'flatDeleteAction',
                        settings: {
                            nameIconButton: 'delete',
                        },
                    },
                }}

                attributes={{
                    number: {
                        title: 'Number flat',
                        propsTableHeader: {
                            tooltip: 'Number flat',
                        },
                    },
                    title: {
                        title: 'Title',
                        propsTableHeader: {
                            tooltip: 'Title',
                        },
                    },

                    number_floor: {
                        title: 'Number floor',
                        propsTableHeader: {
                            tooltip: 'Number floor',
                        },
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "Flat",
                        url: FLATS_ROUTE,
                        searchFields: [
                            'number',
                        ]
                    },
                    pagination: {
                        entityName: "Flat",
                        url: FLATS_ROUTE,
                    }
                }}

                collectionName="Flat"
                actionName="flatsGetAction"
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default FlatsPage;
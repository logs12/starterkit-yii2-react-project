import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import _ from 'lodash';
import Form from '../../../../../common/widgets/form-widget';
import TextInputWidget from '../../../../../common/widgets/text-input-widget';
import ButtonLoadingWidget from '../../../../../common/widgets/button-loading-widget';
import FileInputMultipleWidget from '../../../../../common/widgets/file-input-multiple-widget';
import BackToListButton from '../../../../components/BackToListButton';
import Select2Widget from '../../../../../common/widgets/select2-widget';


import { flatsGetAction } from '../../../../actions/FlatAction';
import {
    FLAT_URL_REQUEST,
    FLATS_ROUTE,
    FLAT_CREATE_ROUTE,
} from "../../../../../common/constants";

const mapToStateProps = (state, ownProps) => ({
    flatId: Number(ownProps.params.id),
    flats: state.Flat.collection,
    flat: _.find(state.Flat.collection, {id: Number(ownProps.params.id)}),
});

const mapDispatchToProps = (dispatch) => ({
    flatsGetAction: bindActionCreators(flatsGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapToStateProps, mapDispatchToProps)

export default class FlatsForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'flatCreateAction';

    componentWillMount() {
        // If entity update
        if (this.props.flatId) {
            if (!this.props.flats.length) {
                this.props.flatsGetAction(this.props.flatId);
            }
            this.formAction = 'flatUpdateAction';
        }
    }

    render () {

        const { flat, flatId, pushToRouter } = this.props;

        return (
            <div className="flat-page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={FLATS_ROUTE} />
                    <Form
                        actionName={this.formAction}
                        entityName="Flat"
                        initModel={flat}
                        attributes={[
                            'number',
                            'title',
                            'number_floor',
                            'floor_id',
                            'floor_title',
                            'photos'
                        ]}
                        url={FLAT_URL_REQUEST(flatId)}>
                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'number'
                                    label = 'Number flat'
                                    initFocused
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'title'
                                    label = 'Title'
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'number_floor'
                                    label = 'Number floor'
                                />
                            </div>

                            {/*<div className="mdl-cell mdl-cell--3-col">
                                <Select2Widget
                                    name="floor_id"
                                    nameHidden="floor_title"
                                    label = 'Floor'
                                    relationGetActionName="floorsGetAction"
                                    relationCollectionName="Floors"
                                    relationFieldId="id"
                                    relationFieldName="title"
                                />
                            </div>*/}

                        </div>

                        {/*<div>
                            <Select2Widget
                                name=""
                                placeholder = 'Address Two'
                                className="mdl-cell mdl-cell--3-col"
                            />
                        </div>*/}

                        <div>
                            <FileInputMultipleWidget
                                type = 'file'
                                name = 'photos'
                                title = 'Select file to upload'
                            />
                        </div>

                        <div className="form-widget__actions mdl-card__actions mdl-card--border mdl-grid">
                            <ButtonLoadingWidget
                                label="Save"
                                type="saveAndBackToList"
                                backToListUrl={FLATS_ROUTE}
                            />
                            { !flatId ?
                                <ButtonLoadingWidget
                                    label="Save & New"
                                    type="saveAndMore"
                                    backToListUrl={FLAT_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}
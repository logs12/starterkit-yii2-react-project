import './style.scss';

import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from "redux";
import * as actions from '../../../../actions/FlatAction';
import Article from '../../../../../common/widgets/article-widget/component';
import BackToListButton from '../../../../components/BackToListButton';
import GalleryLightboxWidget from '../../../../../common/widgets/gallery-lightbox-widget';

import {
    FLATS_ROUTE
} from "../../../../../common/constants";

const mapStateToProps = (state, ownProps) => ({
        flats: state.Flats.collection,
        flat: _.find(state.Flats.collection, {id: Number(ownProps.params.id)}),
});

const mapDispatchToProps = (dispatch) => ({
        flatActions: bindActionCreators(actions, dispatch),
        pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class FlatView extends Component {

    componentWillMount() {
        if (_.isEmpty(this.props.flat)) {
            this.props.flatActions.flatsGetAction();
        }
    }

    render() {

        const { flat, pushToRouter } = this.props;

        return (
            <div className="flat-view-page">
                <Article>

                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={FLATS_ROUTE} />

                    {(!_.isEmpty(flat)) ?
                        <div>
                            <p><b>Number:</b> {flat.number}</p>
                            <p><b>title:</b> {flat.title}</p>
                            <p><b>Number floor one:</b> {flat.number_floor}</p>

                            <GalleryLightboxWidget images={flat.photos} />
                        </div>
                    : null}

                </Article>
            </div>
        )
    }
}
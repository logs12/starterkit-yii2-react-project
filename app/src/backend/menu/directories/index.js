import React, { Component } from 'react';
import {
    DIRECTORIES_ROUTE,
} from '../../../common/constants';

const DirectoriesPage = (props) => {

    return (
        <div className="directories-page">
           { props.location.pathname == DIRECTORIES_ROUTE ?
               <h1>DirectoriesPage</h1> :
               props.children
           }
        </div>
    )
};

export default DirectoriesPage;
import './style.scss';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import _ from 'lodash';
import Form from '../../../../../common/widgets/form-widget/container';
import TextInputWidget from '../../../../../common/widgets/text-input-widget/container';
import ButtonLoadingWidget from '../../../../../common/widgets/button-loading-widget/container';
import FileInputMultipleWidget from '../../../../../common/widgets/file-input-multiple-widget/container';
import BackToListButton from '../../../../components/BackToListButton';

import { roomsGetAction } from '../../../../actions/RoomAction';
import {
    ROOM_URL_REQUEST,
    ROOMS_ROUTE,
    ROOM_CREATE_ROUTE,
} from "../../../../../common/constants";

const mapToStateProps = (state, ownProps) => ({
    roomId: Number(ownProps.params.id),
    roomCollection: state.Room.collection,
    room: _.find(state.Room.collection, {id: Number(ownProps.params.id)}),
});

const mapDispatchToProps = (dispatch) => ({
    roomsGetAction: bindActionCreators(roomsGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapToStateProps, mapDispatchToProps)

export default class RoomsForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'roomCreateAction';

    componentWillMount() {
        // If entity update
        if (this.props.roomId) {
            if (!this.props.roomCollection.length) {
                this.props.roomsGetAction(this.props.roomId);
            }
            this.formAction = 'roomUpdateAction';
        }
    }

    render () {

        const { room, roomId, pushToRouter } = this.props;

        return (
            <div className="room-page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={ROOMS_ROUTE} />
                    <Form
                        actionName={this.formAction}
                        formName="roomForm"
                        model={room}
                        attributes={['size','flooring', 'ceiling_height', 'photos']}
                        url={ROOM_URL_REQUEST(roomId)}>
                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'size'
                                    label = 'Size'
                                    initFocused
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'flooring'
                                    label = 'Flooring'
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'ceiling_height'
                                    label = 'Ceiling height'
                                />
                            </div>

                        </div>

                        <div>
                            <FileInputMultipleWidget
                                type = 'file'
                                name = 'photos'
                                label = 'Select file to upload'
                            />
                        </div>

                        <div className="form-widget__actions mdl-card__actions mdl-card--border mdl-grid">
                            <ButtonLoadingWidget
                                label="Save"
                                type="saveAndBackToList"
                                backToListUrl={ROOMS_ROUTE}
                            />
                            { !roomId ?
                                <ButtonLoadingWidget
                                    label="Save & New"
                                    type="saveAndMore"
                                    backToListUrl={ROOM_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}
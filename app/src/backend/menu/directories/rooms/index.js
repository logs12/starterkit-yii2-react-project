import React from 'react';
import TableWidget from '../../../../common/widgets/table-widget';

import {
    ROOMS_ROUTE,
    ROOM_CREATE_ROUTE,
    ROOM_VIEW_ROUTE,
    ROOM_UPDATE_ROUTE,
} from '../../../../common/constants';


const RoomsPage = () => {
    return (
        <div className="buildings-page">
            <TableWidget
                actionsTableHeader={[
                    {
                        title: 'Add',
                        iconName: 'add',
                        link: ROOM_CREATE_ROUTE,
                    }
                ]}

                rowMenuActions = {{
                    actionView: {
                        title: 'View',
                        url: ROOM_VIEW_ROUTE,
                        settings: {
                            clickRow: true,
                        },
                    },
                    actionUpdate: {
                        title: 'Edit',
                        url: ROOM_UPDATE_ROUTE,
                        settings: {
                            nameIconButton: 'edit',
                        },
                    },
                    actionDelete: {
                        title: 'Delete',
                        dispatchAction: 'buildingDeleteAction',
                        settings: {
                            nameIconButton: 'delete',
                        },
                    },
                }}

                attributes={{
                    type: {
                        title: 'Type room',
                        propsTableHeader: {
                            tooltip: 'State name',
                        },
                    },
                    number_flat:{
                        title: 'Number flat',
                        propsTableHeader: {
                            tooltip: 'Number flat',
                        },
                    },

                    trim:{
                        title: 'Trim',
                        propsTableHeader: {
                            tooltip: 'Trim',
                        },
                    },
                    door:{
                        title: 'Door',
                        propsTableHeader: {
                            tooltip: 'Door',
                        },
                    },
                    door_knob:{
                        title: 'Door knob',
                        propsTableHeader: {
                            tooltip: 'Door knob',
                        },
                    },
                    crown_molding:{
                        title: 'Crown molding',
                        propsTableHeader: {
                            tooltip: 'Crown molding',
                        },
                    },
                    size:{
                        title: 'Size',
                        propsTableHeader: {
                            tooltip: 'Size',
                        },
                    },
                    flooring:{
                        title: 'Flooring',
                        propsTableHeader: {
                            tooltip: 'Flooring',
                        },
                    },
                    ceiling_height:{
                        title: 'Ceiling height',
                        propsTableHeader: {
                            tooltip: 'Ceiling height',
                        },
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "Room",
                        url: ROOMS_ROUTE,
                        searchFields: [
                            'trim',
                            'door',
                            'door_knob',
                            'crown_molding',
                            'size',
                            'flooring',
                            'ceiling_height',
                        ]
                    },
                    pagination: {
                        entityName: "Room",
                        url: ROOMS_ROUTE,
                    }
                }}

                collectionName="Room"
                actionName="roomsGetAction"
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default RoomsPage;
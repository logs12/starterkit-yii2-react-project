import './style.scss';

import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from "redux";
import * as actions from '../../../../actions/BuildingAction';
import Article from '../../../../../common/widgets/article-widget/component';
import BackToListButton from '../../../../components/BackToListButton';
import GalleryLightboxWidget from '../../../../../common/widgets/gallery-lightbox-widget';

import {
    BUILDINGS_ROUTE
} from "../../../../../common/constants";

const mapStateToProps = (state, ownProps) => ({
        buildings: state.Buildings.collection,
        building: _.find(state.Buildings.collection, {id: Number(ownProps.params.id)}),
});

const mapDispatchToProps = (dispatch) => ({
        buildingActions: bindActionCreators(actions, dispatch),
        pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class BuildingView extends Component {

    componentWillMount() {
        if (_.isEmpty(this.props.building)) {
            this.props.buildingActions.buildingsGetAction();
        }
    }

    render() {

        const { building, pushToRouter } = this.props;

        return (
            <div className="building-view-page">
                <Article>

                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={BUILDINGS_ROUTE} />

                    {(!_.isEmpty(building)) ?
                        <div>
                            <p><b>State:</b> {building.state}</p>
                            <p><b>City:</b> {building.city}</p>
                            <p><b>Address one:</b> {building.address_one}</p>
                            <p><b>Address two:</b> {building.address_two}</p>
                            <p><b>Number of floors:</b> {building.floors}</p>

                            <GalleryLightboxWidget images={building.photos} />
                        </div>
                    : null}

                </Article>
            </div>
        )
    }
}
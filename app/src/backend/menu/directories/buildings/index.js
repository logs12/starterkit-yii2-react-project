import React from 'react';
import TableWidget from '../../../../common/widgets/table-widget';

import {
    BUILDINGS_ROUTE,
    BUILDING_CREATE_ROUTE,
    BUILDING_VIEW_ROUTE,
    BUILDING_UPDATE_ROUTE,
} from '../../../../common/constants';

const BuildingsPage = () => {
    return (
        <div className="buildings-page">
            <TableWidget
                actionsTableHeader={[
                    {
                        title: 'Add',
                        iconName: 'add',
                        link: BUILDING_CREATE_ROUTE,
                    }
                ]}

                rowMenuActions = {{
                    actionView: {
                        title: 'View',
                        url: BUILDING_VIEW_ROUTE,
                        settings: {
                            clickRow: true,
                        },
                    },
                    actionUpdate: {
                        title: 'Edit',
                        url: BUILDING_UPDATE_ROUTE,
                        settings: {
                            nameIconButton: 'edit',
                        },
                    },
                    actionDelete: {
                        title: 'Delete',
                        dispatchAction: 'buildingDeleteAction',
                        settings: {
                            nameIconButton: 'delete',
                        },
                    },
                }}

                attributes={{
                    state: {
                        title: 'State name',
                        propsTableHeader: {
                            tooltip: 'State name',
                        },
                    },
                    city: {
                        title: 'City',
                        propsTableHeader: {
                            tooltip: 'City name',
                        },
                    },

                    address_one: {
                        title: 'Address one',
                        propsTableHeader: {
                            tooltip: 'Address one',
                        },
                    },
                    address_two: {
                        title: 'Address two',
                        propsTableHeader: {
                            tooltip: 'Address two',
                        },
                    },
                    floors: {
                        title: 'Number of floors',
                        propsTableHeader: {
                            tooltip: 'Number of floors',
                        },
                        value: (model) => {
                            return model.length;
                        }
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "Building",
                        url: BUILDINGS_ROUTE,
                        searchFields: [
                            'state',
                            'city',
                            'address_one',
                            'address_two',
                            'floors',
                        ]
                    },
                    pagination: {
                        entityName: "Building",
                        url: BUILDINGS_ROUTE,
                    }
                }}

                collectionName="Building"
                actionName="buildingsGetAction"
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default BuildingsPage;
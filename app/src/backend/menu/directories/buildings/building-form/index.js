import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import _ from 'lodash';
import FormWidget from '../../../../../common/widgets/form-widget';
import TextInputWidget from '../../../../../common/widgets/text-input-widget';
import ButtonLoadingWidget from '../../../../../common/widgets/button-loading-widget';
import FileInputMultipleWidget from '../../../../../common/widgets/file-input-multiple-widget/container';
import BackToListButton from '../../../../components/BackToListButton';

import { buildingsGetAction } from '../../../../actions/BuildingAction';
import {
    BUILDING_URL_REQUEST,
    BUILDINGS_ROUTE,
    BUILDING_CREATE_ROUTE,
} from "../../../../../common/constants";

const mapToStateProps = (state, ownProps) => ({
    buildingId: Number(ownProps.params.id),
    buildings: state.Building.collection,
    building: _.find(state.Building.collection, {id: Number(ownProps.params.id)}),
});

const mapDispatchToProps = (dispatch) => ({
    buildingsGetAction: bindActionCreators(buildingsGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapToStateProps, mapDispatchToProps)

export default class BuildingsForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'buildingCreateAction';

    componentWillMount() {
        // If entity update
        if (this.props.buildingId) {
            if (!this.props.buildings.length) {
                this.props.buildingsGetAction(this.props.buildingId);
            }
            this.formAction = 'buildingUpdateAction';
        }
    }

    render () {

        const { building, buildingId, pushToRouter } = this.props;

        return (
            <div className="building-page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={BUILDINGS_ROUTE} />
                    <FormWidget
                        actionName={this.formAction}
                        entityName="Building"
                        initModel={building}
                        attributes={[
                            'state',
                            'city',
                            'address_one',
                            'address_two',
                            'photos',
                        ]}
                        url={BUILDING_URL_REQUEST(buildingId)}>
                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--3-col">
                                <TextInputWidget
                                    name = 'state'
                                    label = 'State'
                                    initFocused
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--3-col">
                                <TextInputWidget
                                    name = 'city'
                                    label = 'City'
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--3-col">
                                <TextInputWidget
                                    name = 'address_one'
                                    label = 'Address One'
                                />
                            </div>


                            <div className="mdl-cell mdl-cell--3-col">
                                <TextInputWidget
                                    name = 'address_two'
                                    label = 'Address Two'
                                />
                            </div>

                        </div>

                       {/*   <div>
                            <FileInputMultipleWidget
                                name = 'photos'
                                type = 'file'
                                label = 'Select file to upload'
                            />
                        </div>*/}

                        <div className="form-widget__actions mdl-card__actions mdl-card--border mdl-grid">
                            <ButtonLoadingWidget
                                label="Save"
                                type="saveAndBackToList"
                                backToListUrl={BUILDINGS_ROUTE}
                            />
                            { !buildingId ?
                                <ButtonLoadingWidget
                                    label="Save & New"
                                    type="saveAndMore"
                                    backToListUrl={BUILDING_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </FormWidget>
                </div>
            </div>
        );
    }
}
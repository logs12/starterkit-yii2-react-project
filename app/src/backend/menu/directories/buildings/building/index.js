import './style.scss';

import React, { Component, createElement } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from "redux";
import * as buildingActions from '../../../../actions/BuildingAction';
import Article from '../../../../../common/widgets/article-widget/component';
import BackToListButton from '../../../../components/BackToListButton';
import GalleryLightboxWidget from '../../../../../common/widgets/gallery-lightbox-widget';
import CategoryToContentWidget from '../../../../../common/widgets/category-to-content-widget';
import { getBuilding } from '../../../../selectors/SelectedBuilding';

import {
    BUILDINGS_ROUTE,
    FLAT_VIEW_ROUTE,
} from "../../../../../common/constants";

const mapStateToProps = (state, ownProps) => ({
    building: getBuilding(state, [Number(ownProps.params.id)]),
    currentRoute: state.routing.locationBeforeTransitions.pathname,
});

const mapDispatchToProps = (dispatch) => ({
        buildingActions: bindActionCreators(buildingActions, dispatch),
        pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class BuildingView extends Component {

    componentWillMount() {
        if (_.isEmpty(this.props.building)) {
            this.props.buildingActions.buildingsGetAction();
        }
    }

    render() {

        const { building, pushToRouter, currentRoute } = this.props;

        return (
            <div className="building-view-page">
                <Article>

                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={BUILDINGS_ROUTE} />

                    {(!_.isEmpty(building)) ?
                        <div>
                            <p><b>State:</b> {building.state}</p>
                            <p><b>City:</b> {building.city}</p>
                            <p><b>Address one:</b> {building.address_one}</p>
                            <p><b>Address two:</b> {building.address_two}</p>
                            <p><b>Number of floors:</b> {building.floors.length}</p>

                            <GalleryLightboxWidget images={building.photos} />

                            <CategoryToContentWidget
                                categoryCollection={building.floors}
                                contentCollectionName="Flat"
                                actionName="flatsGetAction"
                                conditionFieldName="floor_id"
                                urlContentView={FLAT_VIEW_ROUTE}
                                currentRoute={currentRoute}
                                categoryTitle="Floors"
                                contentTitle="Flats"
                            />
                        </div>
                    : null}

                </Article>
            </div>
        )
    }
}
import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from "redux";
import * as actions from '../../../../actions/UserAction';
import Article from '../../../../../common/widgets/article-widget/component';
import BackToListButton from '../../../../components/BackToListButton';

import {
    USERS_ROUTE
} from "../../../../../common/constants";

const mapStateToProps = (state, ownProps) => ({
    users: state.Users.collection,
    user: _.find(state.Users.collection, {id: Number(ownProps.params.id)}),
});

const mapDispatchToProps = (dispatch) => ({
    userActions: bindActionCreators(actions, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class UserView extends Component {

    componentWillMount() {
        if (_.isEmpty(this.props.user)) {
            this.props.userActions.usersGetAction();
        }
    }

    render() {

        const { user, pushToRouter } = this.props;

        return (
            <div className="user-view-page">
                <Article>

                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={USERS_ROUTE} />

                    {(!_.isEmpty(user)) ?
                        <div>
                            <p>{user.first_name}</p>
                            <p>{user.second_name}</p>
                            <p>{user.third_name}</p>
                            <p>{user.email}</p>
                            <p>{user.phone}</p>
                        </div>
                    : null}

                </Article>
            </div>
        )
    }
}
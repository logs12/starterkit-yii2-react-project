import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import { usersGetAction } from '../../../../actions/UserAction';
import _ from 'lodash';

import Form from '../../../../../common/widgets/form-widget/container';
import TextInputWidget from '../../../../../common/widgets/text-input-widget/container';
import ButtonLoadingWidget from '../../../../../common/widgets/button-loading-widget/container';
import BackToListButton from '../../../../components/BackToListButton';

import {
    USERS_ROUTE,
    USER_CREATE_ROUTE,
    USER_URL_REQUEST,
} from "../../../../../common/constants";

const mapToStateProps = (state, ownProps) => ({
    userId: Number(ownProps.params.id),
    users: state.Users.collection,
    user: _.find(state.Users.collection, {id: Number(ownProps.params.id)}),
});

const mapDispatchToProps = (dispatch) => ({
    usersGetAction: bindActionCreators(usersGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapToStateProps, mapDispatchToProps)

export default class UserForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'userCreateAction';

    componentWillMount() {
        // If entity update
        if (this.props.userId) {
            if (!this.props.users.length) {
                this.props.usersGetAction(this.props.userId);
            }
            this.formAction = 'userUpdateAction';
        }
    }

    render () {

        const { pushToRouter, user, userId } = this.props;

        return (
            <div className="user-page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={USERS_ROUTE} />
                    <Form
                        actionName={this.formAction}
                        formName="userForm"
                        model={user}
                        attributes={[
                            'first_name',
                            'second_name',
                            'third_name',
                            'phone',
                            'email',
                            'password',
                        ]}
                        url={USER_URL_REQUEST(userId)}>
                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'first_name'
                                    placeholder = 'First name'
                                    initFocused
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'second_name'
                                    placeholder = 'Second name'
                                />
                            </div>


                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'third_name'
                                    placeholder = 'Third name'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'phone'
                                    placeholder = 'Phone'
                                    className="mdl-cell mdl-cell--4-col"
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'email'
                                    placeholder = 'Email'
                                    className="mdl-cell mdl-cell--4-col"
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'password'
                                    placeholder = 'Password'
                                    className="mdl-cell mdl-cell--4-col"
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <ButtonLoadingWidget
                                label="Save"
                                type="saveAndBackToList"
                                backToListUrl={USERS_ROUTE}
                            />
                            { !userId ?
                                <ButtonLoadingWidget
                                    label="Save & New"
                                    type="saveAndMore"
                                    backToListUrl={USER_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}
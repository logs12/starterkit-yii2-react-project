import { createSelector } from 'reselect';
import _ from 'lodash';

const flatsSelector = state => state.flats;

const selectedFlatsSelector = state => state.selectedFlatIds;

const getFlats = (flats, selectedFlatIds) => {
    return _.filter(
        flats,
        flat => _.includes(selectedFlatIds, flat.id),
    );
};

export default createSelector(
    flatsSelector,
    selectedFlatsSelector,
    getFlats,
);

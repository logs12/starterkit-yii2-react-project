import { createSelector } from 'reselect';
import _ from 'lodash';

const floorsSelector = state => state.Floors;

const selectedFloorsSelector = state => state.selectedFloorIds;

const getFloors = (floors, selectedFloorIds) => {
    return _.filter(
        floors,
        floor => _.includes(selectedFloorIds, floor.id),
    );
};

export default createSelector(
    floorsSelector,
    selectedFloorsSelector,
    getFloors,
);

import { createSelector } from 'reselect';
import _ from 'lodash';

/**
 * Selected Collection Building
 * @param state
 * @returns {*}
 */
const buildingsSelector = (state) => {
    return state.Building.collection;
};

/**
 * id for filtering collection
 * @param state
 * @param selectedBuildingId
 */
const selectedBuildingSelector = (state, selectedBuildingId) => (Number(selectedBuildingId));

/**
 * Filtering collection
 * @param buildings
 * @param selectedBuildingId
 * @returns {*|{}|T}
 */
const getBuildingSelector = (buildings, selectedBuildingId) => {
    return _.find(buildings, {id: selectedBuildingId});
};


/**
 * Selectors building
 * @param state
 */
const selectedBuildingsSelector = state => state.Buildings.selectedBuildingIds;

/**
 *
 * @param buildings
 * @param selectedBuildingIds
 * @returns {*}
 */
const getBuildingsSelector = (buildings, selectedBuildingIds) => {

    return _.filter(
        buildings,
        building => _.includes(selectedBuildingIds, building.id),
    );
};

export const getBuilding = createSelector(
    buildingsSelector,
    selectedBuildingSelector,
    getBuildingSelector,
);


export const getBuildings =  createSelector(
    buildingsSelector,
    selectedBuildingsSelector,
    getBuildingsSelector,
);

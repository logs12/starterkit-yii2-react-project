//========== /Constants Building ==========//
export const ROOMS_GET = 'ROOMS_GET';
export const ROOM_DELETE = 'ROOM_DELETE';
export const ROOM_FORM_WIDGET_REQUEST = 'ROOM_WIDGET_FORM_REQUEST';
export const ROOM_FORM_WIDGET_SUCCESS = 'ROOM_WIDGET_FORM_SUCCESS';
export const ROOM_FORM_WIDGET_ERROR = 'ROOM_WIDGET_FORM_ERROR';
export const ROOM_MODEL_INIT = 'ROOM_MODEL_INIT';
export const ROOM_MODEL_UPDATE = 'ROOM_MODEL_UPDATE';
export const ROOM_FORM_MODEL_ERROR = 'ROOM_FORM_MODEL_ERROR';

// Constants url request
export const ROOM_GET_URL_REQUEST = `/api-internal/room`;
export const ROOM_URL_REQUEST = (id = null) => (id) ? `/api-internal/room/${id}` : `/api-internal/room`;
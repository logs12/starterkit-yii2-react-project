//========== /Constants Building ==========//
export const BUILDINGS_GET = 'BUILDINGS_GET';
export const BUILDING_VIEW = 'BUILDING_VIEW';
export const BUILDING_CREATE = 'BUILDING_CREATE';
export const BUILDING_UPDATE = 'BUILDING_UPDATE';
export const BUILDING_DELETE = 'BUILDING_DELETE';
export const BUILDING_FORM_WIDGET_REQUEST = 'BUILDING_FORM_WIDGET_REQUEST';
export const BUILDING_FORM_WIDGET_SUCCESS = 'BUILDING_FORM_WIDGET_SUCCESS';
export const BUILDING_FORM_WIDGET_ERROR = 'BUILDING_FORM_WIDGET_ERROR';
export const BUILDING_MODEL_INIT = 'BUILDING_MODEL_INIT';
export const BUILDING_MODEL_UPDATE = 'BUILDING_MODEL_UPDATE';
export const BUILDING_FORM_MODEL_ERROR = 'BUILDING_FORM_MODEL_ERROR';

// Constants url request
export const BUILDING_GET_URL_REQUEST = `/api-internal/building`;
export const BUILDING_URL_REQUEST = (id = null) => (id) ? `/api-internal/building/${id}` : `/api-internal/building`;
import './style.scss';

import React, {Component} from "react";
import Button from "react-mdl/lib/Button";
import { push } from 'react-router-redux';

/**
 * Handle click button
 * @param pushToRouter
 * @param routeBackToList
 */
const handleBackToListButton = (pushToRouter, routeBackToList) => {
    pushToRouter(routeBackToList);
};

/**
 * Component Button back to list
 * @param pushToRouter
 * @param routeBackToList {string} - url back to list
 * @returns {XML}
 * @constructor
 */
const BackToListButton = ({ pushToRouter, routeBackToList }) => {
    return (
        <div className = "back-to-list">
            <Button className = "back-to-list__button"
                    ripple
                    onClick={() => { handleBackToListButton(pushToRouter, routeBackToList) }}>
                Back to list
            </Button>
        </div>
    )
};

export default BackToListButton;
import './style.scss';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Layout, Header, Content, Drawer, Navigation } from '../../../common/widgets/layout-widget';

import DrawerHeader from '../../containers/DrawerHeader';
import DrawerMenu from './DrawerMenu';
import HeaderMenu from './HeaderMenu';

import NavLink from '../../../common/widgets/nav-link/component';
import ProgressBarWidget from '../../../common/widgets/progress-bar-widget';
import SnackbarWidget from '../../../common/widgets/snackbar-widget';

import {
    USERS_ROUTE,
    BUILDINGS_ROUTE,
    FLOORS_ROUTE,
    FLATS_ROUTE,
    ROOMS_ROUTE,
} from '../../../common/constants';

const mapStateToProps = state => ({
    pathname: state.routing.locationBeforeTransitions.pathname,
});

@connect(mapStateToProps)

export default class MainPageLayout extends Component {

    /**
     *
     * @type {{navLinks: Array, title: null, fixedDrawer: boolean, closeDrawer: boolean}}
     */
    state = {
        navLinks: [],
        title: null,
        fixedDrawer: false,
        closeDrawer:false,
    };

    /**
     * Handle click navigation link drawer menu
     */
    onClickNavLinkDrawerMenu() {
        this.setState({
            closeDrawer:true,
        });
    }

    /**
     * @param pathname
     */
    renderSubComponentFromPathname(pathname) {
        if (pathname.indexOf("administration") !== -1) {
            this.setState({
                navLinks: [
                    <NavLink
                        key="users"
                        to={USERS_ROUTE}
                        className="drawer-menu__nav-link"
                    >
                        Users
                    </NavLink>
                ],
                title: 'Administration',
                fixedDrawer: false,
            });
        } else if (pathname.indexOf("directories") !== -1) {
            this.setState({
                navLinks: [
                    <NavLink
                        key="buildings"
                        to={BUILDINGS_ROUTE}
                        className="drawer-menu__nav-link"
                    >
                        Buildings
                    </NavLink>,
                    <NavLink
                        key="floors"
                        to={FLOORS_ROUTE}
                        className="drawer-menu__nav-link"
                    >
                        Floors
                    </NavLink>,
                    <NavLink
                        key="flats"
                        to={FLATS_ROUTE}
                        className="drawer-menu__nav-link"
                    >
                        Flats
                    </NavLink>,
                    <NavLink
                        key="rooms"
                        to={ROOMS_ROUTE}
                        className="drawer-menu__nav-link"
                    >
                        Rooms
                    </NavLink>

                ],
                title: 'Directories',
                fixedDrawer: false,
            });
        } else {
            this.setState({
                navLinks: [],
                fixedDrawer:true,
                title: 'Home',
            });
        }
    }

    componentWillMount() {
        this.renderSubComponentFromPathname(this.props.pathname);
    }

    componentWillReceiveProps(nextProps) {
        this.renderSubComponentFromPathname(nextProps.pathname);
    }

    render() {

        const { title, navLinks, fixedDrawer, closeDrawer } = this.state;

        return (
            <Layout fixedHeader fixedDrawer={fixedDrawer} className="menu-page-layout"  closeDrawer = {closeDrawer}>
                <Header title={
                        <span style={{ color: '#ddd' }}>
                        {title}
                        </span>
                    }>
                    <HeaderMenu navLinks={navLinks}/>
                </Header>
                <ProgressBarWidget />
                <section className="breadcrumbs"></section>
                <Drawer className="mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
                    <DrawerHeader />
                    <DrawerMenu onClick={::this.onClickNavLinkDrawerMenu} />
                </Drawer>
                <Content component="main">{this.props.children}</Content>
                <SnackbarWidget />
            </Layout>
        );
    }
};


import React, { Component } from 'react';
import { Navigation } from 'react-mdl/lib/Layout';
import NavLink from '../../../common/widgets/nav-link/component';


export default class HeaderMenu extends Component {

    render () {

        const { navLinks } = this.props;

        return (
            <Navigation className="header-menu mdl-color--blue-grey-800">
                { navLinks.map((navLink) => navLink)}
            </Navigation>
        )
    }
}
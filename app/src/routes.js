import React from 'react';
import { Route, IndexRoute } from 'react-router';
import { render } from 'react-dom';

// Layouts Backend
import MainPageLayout from './backend/layouts/MainPageLayout';

// Backend components
import DashboardPage from './backend/pages/dashboard-page';
import AdministrationPage from './backend/menu/administration';
import UsersPage from './backend/menu/administration/users';
import UserViewPage from './backend/menu/administration/users/user';
import UserFormPage from './backend/menu/administration/users/user-form';
import DirectoriesPage from './backend/menu/directories';

import BuildingsPage from './backend/menu/directories/buildings';
import BuildingViewPage from './backend/menu/directories/buildings/building';
import BuildingFormPage from './backend/menu/directories/buildings/building-form';

import FloorsPage from './backend/menu/directories/floors';
import FloorViewPage from './backend/menu/directories/floors/floor';
import FloorFormPage from './backend/menu/directories/floors/floor-form';

import FlatsPage from './backend/menu/directories/flats';
import FlatViewPage from './backend/menu/directories/flats/flat';
import FlatFormPage from './backend/menu/directories/flats/flat-form';

import RoomsPage from './backend/menu/directories/rooms';
import RoomViewPage from './backend/menu/directories/rooms/room';
import RoomFormPage from './backend/menu/directories/rooms/room-form';

// Common components
import LoginPage from './common/pages/login-page';
import SignUpPage from './common/pages/sign-up-page';
import ErrorPage from './common/pages/error-page';
import NotFoundPage from './common/pages/not-found-page';
import ApplicationContainer from './common/containers/ApplicationContainer';

// constants react-redux-router app
import {
    ADMIN_ROUTE,
    ADMINISTRATION_ROUTE,
    USERS_ROUTE,
    USER_VIEW_ROUTE,
    USER_CREATE_ROUTE,
    USER_UPDATE_ROUTE,
    USER_DELETE_ROUTE,

    DIRECTORIES_ROUTE,

    BUILDINGS_ROUTE,
    BUILDING_VIEW_ROUTE,
    BUILDING_CREATE_ROUTE,
    BUILDING_UPDATE_ROUTE,
    BUILDING_DELETE_ROUTE,

    FLOORS_ROUTE,
    FLOOR_CREATE_ROUTE,
    FLOOR_VIEW_ROUTE,
    FLOOR_UPDATE_ROUTE,
    FLOOR_DELETE_ROUTE,

    FLATS_ROUTE,
    FLAT_CREATE_ROUTE,
    FLAT_VIEW_ROUTE,
    FLAT_UPDATE_ROUTE,
    FLAT_DELETE_ROUTE,

    ROOMS_ROUTE,
    ROOM_CREATE_ROUTE,
    ROOM_VIEW_ROUTE,
    ROOM_UPDATE_ROUTE,
    ROOM_DELETE_ROUTE,

} from './common/constants';


export const routes = (
    <Route>
        {/*Frontend*/}{/*
        <Route path="/" component={ApplicationContainer(FrontendLayout, false)}>
            <IndexRoute component={FrontendHomePage}/>
            <Route path="test" component={TestPage}/>
        </Route>*/}

        {/*Backend*/}
        <Route path={ADMIN_ROUTE} component={ApplicationContainer(MainPageLayout)}>
            <IndexRoute component={DashboardPage}/>
            <Route path={ADMINISTRATION_ROUTE} component={AdministrationPage}/>
            <Route path={USERS_ROUTE} component={UsersPage}/>
            <Route path={USER_CREATE_ROUTE} component={UserFormPage}/>
            <Route path={USER_VIEW_ROUTE(':id')} component={UserViewPage}/>
            <Route path={USER_UPDATE_ROUTE(':id')} component={UserFormPage}/>
            <Route path={USER_DELETE_ROUTE(':id')} component={UsersPage}/>

            {/* Directories */}
            <Route path={DIRECTORIES_ROUTE} component={DirectoriesPage} >
                <Route path={BUILDINGS_ROUTE} component={BuildingsPage}/>
                <Route path={BUILDING_CREATE_ROUTE} component={BuildingFormPage}/>
                <Route path={BUILDING_VIEW_ROUTE(':id')} component={BuildingViewPage}/>
                <Route path={BUILDING_UPDATE_ROUTE(':id')} component={BuildingFormPage}/>
                <Route path={BUILDING_DELETE_ROUTE(':id')} component={BuildingsPage}/>


                <Route path={FLOORS_ROUTE} component={FloorsPage}/>
                <Route path={FLOOR_CREATE_ROUTE} component={FloorFormPage}/>
                <Route path={FLOOR_VIEW_ROUTE(':id')} component={FloorViewPage}/>
                <Route path={FLOOR_UPDATE_ROUTE(':id')} component={FloorFormPage}/>
                <Route path={FLOOR_DELETE_ROUTE(':id')} component={FloorsPage}/>

                <Route path={FLATS_ROUTE} component={FlatsPage}/>
                <Route path={FLAT_CREATE_ROUTE} component={FlatFormPage}/>
                <Route path={FLAT_VIEW_ROUTE(':id')} component={FlatViewPage}/>
                <Route path={FLAT_UPDATE_ROUTE(':id')} component={FlatFormPage}/>
                <Route path={FLAT_DELETE_ROUTE(':id')} component={FlatsPage}/>

                <Route path={ROOMS_ROUTE} component={RoomsPage}/>
                <Route path={ROOM_CREATE_ROUTE} component={RoomFormPage}/>
                <Route path={ROOM_VIEW_ROUTE(':id')} component={RoomViewPage}/>
                <Route path={ROOM_UPDATE_ROUTE(':id')} component={RoomFormPage}/>
                <Route path={ROOM_DELETE_ROUTE(':id')} component={RoomsPage}/>
            </Route>

        </Route>

        {/*Common*/}
        <Route path="/login" component={ApplicationContainer(LoginPage, false)}/>
        <Route path="/sign-up" component={ApplicationContainer(SignUpPage, false)}/>

        <Route path="/error" component={ErrorPage}/>
        <Route path='*' component={NotFoundPage}/>
    </Route>
);
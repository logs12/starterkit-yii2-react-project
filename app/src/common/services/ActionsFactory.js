import { loginAction, logOutAction } from '../../common/actions/AuthAction';

import * as userActions from '../../backend/actions/UserAction';
import * as buildingActions from '../../backend/actions/BuildingAction';
import * as floorActions from '../../backend/actions/FloorAction';
import * as flatActions from '../../backend/actions/FlatAction';
import * as roomActions from '../../backend/actions/RoomAction';


/**
 * Список action
 * @type {{authAction: authAction}}
 */
const ListActions = {
    loginAction: loginAction,
    logOutAction: logOutAction,

    usersGetAction: userActions.usersGetAction,
    userCreateAction: userActions.userCreateAction,
    userUpdateAction: userActions.userUpdateAction,
    userDeleteAction: userActions.userDeleteAction,

    buildingsGetAction: buildingActions.buildingsGetAction,
    buildingCreateAction: buildingActions.buildingCreateAction,
    buildingUpdateAction: buildingActions.buildingUpdateAction,
    buildingDeleteAction: buildingActions.buildingDeleteAction,

    floorsGetAction: floorActions.floorsGetAction,
    floorCreateAction: floorActions.floorCreateAction,
    floorUpdateAction: floorActions.floorUpdateAction,
    floorDeleteAction: floorActions.floorDeleteAction,

    flatsGetAction: flatActions.flatsGetAction,
    flatCreateAction: flatActions.flatCreateAction,
    flatUpdateAction: flatActions.flatUpdateAction,
    flatDeleteAction: flatActions.flatDeleteAction,

    roomsGetAction: roomActions.roomsGetAction,
    roomCreateAction: roomActions.roomCreateAction,
    roomUpdateAction: roomActions.roomUpdateAction,
    roomDeleteAction: roomActions.roomDeleteAction,
};

/**
 * Фабричная функция actions
 * @param nameAction - name action
 * @returns {*}
 */
export default function action(nameAction) {

    if (ListActions[nameAction] != undefined) {
        return ListActions[nameAction];
    } else new Error(`${nameAction} not found`);
}
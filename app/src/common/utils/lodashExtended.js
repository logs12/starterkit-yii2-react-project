import * as lodashExtended from 'lodash';
module.exports = lodashExtended;

/**
 * Comparison of two collections
 * @param collectionOne
 * @param collectionTwo
 * @returns {boolean}
 */
const compare = (collectionOne, collectionTwo) => {
    return collectionOne.length == collectionTwo.length && collectionOne.every((item, index)=>{
            return _.isEqual(item, collectionTwo[index]);
        })
};


lodashExtended.mixin({
    compare: compare
});

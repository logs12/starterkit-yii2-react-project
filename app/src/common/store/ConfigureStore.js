import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from '../../common/reducers/RootReducer';
import ThunkMiddleware from '../../common/middlewares/ThunkMiddleware';
import createLogger from 'redux-logger';
import PromiseMiddleware from '../../common/middlewares/PromiseMiddleware';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import DevTools from '../../DevTools'

export default function configureStore() {
    const storeProduction = compose(
        applyMiddleware(ThunkMiddleware),
        applyMiddleware(PromiseMiddleware),
        applyMiddleware(routerMiddleware(browserHistory)),
    )(createStore)(rootReducer);

    const storeDevelopment = compose(
        applyMiddleware(ThunkMiddleware),
        applyMiddleware(PromiseMiddleware),
        applyMiddleware(routerMiddleware(browserHistory)),
        applyMiddleware(createLogger()),
        DevTools.instrument()
        //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    )(createStore)(rootReducer);

    if (NODE_ENV == 'production') {
        return storeProduction;
    }
    return storeDevelopment;
}
import './style.scss';
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const ObfuscatorWidget = (props) => {

    const { isVisible, handleClickObfuscator } = props;

    const classes = classNames('obfuscator-widget', {
        'obfuscator-widget--visible': isVisible
    });

    return (
        <div className={classes} onClick={handleClickObfuscator}></div>
    );
};

ObfuscatorWidget.defaultProps = {
    isVisible: false,
};

ObfuscatorWidget.PropTypes = {
    isVisible: PropTypes.bool.isRequired,
    handleClickObfuscator: PropTypes.func,
};

export default ObfuscatorWidget;
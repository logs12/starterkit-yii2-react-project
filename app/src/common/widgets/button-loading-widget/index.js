import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';
import ButtonLoadingWidgetHoc from './container';

/**
     <ButtonLoadingWidget
        label="Save"
        type="saveAndBackToList"
        backToListUrl={BUILDINGS_ROUTE}
     />
 */
export default class ButtonLoadingWidget extends Component {

    static defaultProps = {
        entityName: null,
        initFocused: false,
    };

    /**
     * Инициализируем контроль типов свойств контекста
     * @type {{url: *}}
     */
    static contextTypes = {
        entityNameForm: PropTypes.string.isRequired,
        formName: PropTypes.string.isRequired,
    };

    render () {
        const {
            label,
            type,
            backToListUrl,
        } = this.props;

        const {
            entityNameForm,
            formName,
        } = this.context;

        return  createElement(ButtonLoadingWidgetHoc(
            label,
            type,
            backToListUrl,
            entityNameForm,
            formName
        ))
    }
};
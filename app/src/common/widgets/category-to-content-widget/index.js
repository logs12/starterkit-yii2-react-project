import './style.scss';

import React, { createElement } from 'react';
import PaginationWidget from '../pagination-widget/container';
import Container from './container';

/**
 * Widget for outputting data by category
 * @param {Object} props
    {
        categoryCollection - collection category,
        collectionContentName - name collection content,
        actionName - name action get content,
        conditionFieldName - field name condition,
        urlContentView - content url view,
        currentRoute - current route,
        categoryTitle - category title,
        contentTitle - content title,
    }
 *
 *
 *
 * @returns {XML}
 * @constructor
 */
const CategoryToContentWidget = props => {

    const {
        categoryCollection,
        contentCollectionName,
        actionName,
        conditionFieldName,
        urlContentView,
        currentRoute,
        categoryTitle,
        contentTitle,
    } = props;

    return (
        <div className="category-to-content-widget">

            {createElement(Container(
                    categoryCollection,
                    contentCollectionName,
                    actionName,
                    conditionFieldName,
                    urlContentView,
                    categoryTitle,
                    contentTitle
                )
            )}

            <div className="category-to-content-widget__pagination">
                {
                    createElement(PaginationWidget({
                        entityName: contentCollectionName,
                        url: currentRoute,
                    }))
                }
            </div>
        </div>
    )
};

export default CategoryToContentWidget;
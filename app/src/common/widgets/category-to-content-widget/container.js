import './style.scss';

import React, { Component, createElement } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import classNames from 'classnames';
import ActionFactory from '../../../common/services/ActionsFactory';
import { push } from 'react-router-redux';


/**
 * HOC component container
 * @param categoryCollection
 * @param contentCollectionName
 * @param actionName
 * @param conditionFieldName
 * @param urlContentView
 * @param categoryTitle
 * @param contentTitle
 * @returns {Container}
 * @constructor
 */
export default function Container(
    categoryCollection,
    contentCollectionName,
    actionName,
    conditionFieldName,
    urlContentView,
    categoryTitle,
    contentTitle
) {

    const mapStateToProps = (state, ownProps) => ({
        categoryTitle: categoryTitle,
        contentTitle: contentTitle,
        urlContentView: urlContentView,
        categoryCollection: categoryCollection,
        contentCollection: state[contentCollectionName].collection,
        condition: state.routing.locationBeforeTransitions.search,
    });

    const mapDispatchToProps = (dispatch) => ({
        getContentCollection: bindActionCreators(ActionFactory(actionName), dispatch),
        pushToRouter: bindActionCreators(push, dispatch),
    });

    @connect(mapStateToProps, mapDispatchToProps)
    class Container extends Component {

        state = {
            selectedCategoryEntityItem: categoryCollection[0].id,
        };

        /**
         *
         */
        componentWillMount() {
            this.props.getContentCollection(
                {condition: `?condition[${conditionFieldName}]=${this.state.selectedCategoryEntityItem}&${this.props.condition.substr(1)}`}
            );
        }

        /**
         *
         * @param {Object} nextProps
         */
        componentWillReceiveProps(nextProps) {
            if (nextProps.condition !=this.props.condition) {
                this.props.getContentCollection(
                    {condition: `?condition[${conditionFieldName}]=${this.state.selectedCategoryEntityItem}&${nextProps.condition.substr(1)}`}
                )
            }
        }

        /**
         *  Get content selected category
         * @param event
         * @param {Number} categoryNumber - selected category number
         */
        handlerSelectNumberCategory(event, categoryNumber) {
            event.preventDefault();
            this.props.getContentCollection({condition: `?condition[${conditionFieldName}]=${categoryNumber}`});
            this.setState({
                selectedCategoryEntityItem: categoryNumber,
            });

        }

        /**
         *  Redirect in view page selected content
         * @param event
         * @param {Number} contentId
         */
        handlerSelectedEntityItem(event, contentId) {
            event.preventDefault();
            this.props.pushToRouter(this.props.urlContentView(contentId));
        }

        /**
         *  Generate content containers
         * @param {collection} contentCollection
         * @returns {Array}
         */
        generateContentContainers(contentCollection) {

            let contentContainer = [];
            const countRow = 4;
            contentCollection.forEach((content, index) => {
                if (index % countRow === 0) {
                    contentContainer.push(
                        <div key={index} className="mdl-grid">
                            {this.generateContentItem(contentCollection, index, countRow)}
                        </div>
                    );
                }
            });
            return contentContainer;
        }

        /**
         *  Generate content item
         * @param {collection} contentCollection
         * @param {number} index
         * @param {number} countRow
         * @returns {Array}
         */
        generateContentItem(contentCollection, index, countRow) {

            let contentContainerItem = [];
            let countIteration = index+countRow;
            if (!contentCollection[countIteration]) {
                countIteration = contentCollection.length;
            }

            for (let i = index; i < countIteration; i++) {
                contentContainerItem.push(
                    <div key={i}
                         className="category-to-content-widget__content-item mdl-cell--4-col-tablet mdl-cell--4-col-phone"
                         onClick={(event) => {
                             ::this.handlerSelectedEntityItem(event, contentCollection[i].id)
                         }}
                    >
                        {contentCollection[i].photos.length ?
                            <img src={contentCollection[i].photos[0].path}/>
                            :
                            <i  className="fa fa-picture-o"
                                aria-hidden="true"
                            ></i>
                        }
                        <span className="category-to-content-widget__content-container-title">Number flat: {contentCollection[i].number}</span>
                    </div>
                );
            }

            return contentContainerItem;
        }

        render() {

            const {
                categoryCollection,
                contentCollection,
                categoryTitle,
                contentTitle,
            } = this.props;

            let { selectedCategoryEntityItem } = this.state;

            let categoryContainers = [];
            let contentContainers = {};

            if (categoryCollection) {
                categoryCollection.forEach((category, index) => {
                    let categoryNumber = category.id;
                    let classNameCategoryContainer = classNames('category-to-content-widget__category-item', {
                        'category-to-content-widget__category-item--active': category.id == selectedCategoryEntityItem
                    });
                    categoryContainers.push(
                        <li key={index}
                            className={classNameCategoryContainer}
                            onClick={(event) => {::this.handlerSelectNumberCategory(event, categoryNumber)}}>

                            #{category.number}
                        </li>
                    );
                });

                if (contentCollection) {
                    contentContainers = this.generateContentContainers(contentCollection);
                }
            }

            return(
                <div className="mdl-grid">
                    <div className="category-to-content-widget__category mdl-cell mdl-cell--2-col mdl-cell--6-col-tablet mdl-cell--6-col-phone">
                        <h4>{categoryTitle}</h4>
                        <ul className="category-to-content-widget__category-container">
                            {categoryContainers}
                        </ul>
                    </div>
                    <div className="category-to-content-widget__content mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet mdl-cell--6-col-phone">
                        <h4>{contentTitle}:</h4>
                        <div className="category-to-content-widget__content-container">
                            {contentContainers}
                        </div>
                    </div>
                </div>
            )
        }

    }
    return Container;
}
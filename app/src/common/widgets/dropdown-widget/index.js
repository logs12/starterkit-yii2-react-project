import './style.scss';
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const DropDownWidget = props => {

    const { showDropDown } = props;
    const className = classNames('dropdown-widget', {
        'dropdown-widget--visible': showDropDown
    });

    return (
        <ul className={className}>
            {props.children}
        </ul>
    )
};

DropDownWidget.PropTypes = {
    showDropDown: PropTypes.bool
};

export default DropDownWidget;
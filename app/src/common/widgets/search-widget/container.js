import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import {  } from "./actions";
import SearchWidgetComponent from './component';
import { push } from "react-router-redux";
import { searchWidgetUpdateAction } from "./actions";


export default function SearchWidget(options) {

    let entityName = null,
        url = null,
        searchFields = null,
        inputTimer = null;

    if (options.hasOwnProperty('entityName')) {
        entityName = options.entityName;
    } else {
        new Error('Not found parameters "entityName" in SearchWidget');
    }

    if (options.hasOwnProperty('url')) {
        url = options.url;
    } else {
        new Error('Not found parameters "url" in SearchWidget');
    }

    if (options.hasOwnProperty('searchFields')) {
        searchFields = options.searchFields;
    } else {
        new Error('Not found parameters "searchFields" in SearchWidget');
    }


    const mapStateToProps = (state) => ({
        searchValue: state[options.entityName].search.searchValue,
    });

    const mapDispatchToProps = (dispatch) => ({
        searchWidgetUpdateAction: bindActionCreators(searchWidgetUpdateAction, dispatch),
        push: bindActionCreators(push, dispatch),
    });

    @connect(mapStateToProps, mapDispatchToProps)
    class SearchWidgetContainer extends Component {

        componentWillMount() {
            if (this.props.searchValue == null) {
                this.props.push(url);
            }
        }

        /**
         * Handle change search input
         * @param event
         */
        onChangeSearchInput (event) {
            event.preventDefault();
            let searchGetParametres = '?',
                targetValue = event.target.value;

            options.searchFields.forEach((searchField, index) => {
                if (index < options.searchFields.length - 1) {
                    searchGetParametres += `${encodeURIComponent(`condition[${searchField}]`)}=${encodeURIComponent(targetValue)}&`;
                } else {
                    searchGetParametres += `${encodeURIComponent(`condition[${searchField}]`)}=${encodeURIComponent(targetValue)}`;
                }
            });


            clearTimeout(this.inputTimer);
            this.inputTimer = setTimeout(() => {
                this.props.searchWidgetUpdateAction({
                    searchValue: targetValue,
                });
                this.props.push(`${url}${searchGetParametres}`);
            }, 400);
        }

        render() {
            return (
                <SearchWidgetComponent
                    onChange={::this.onChangeSearchInput}
                />
            );
        }
    }

    return SearchWidgetContainer;
}

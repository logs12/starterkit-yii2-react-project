import { SEARCH_WIDGET_UPDATE } from '../../constants';

const initialState = {
  searchValue: null,
};

export default function SearchWidgetReducer(state = initialState, action) {
    switch(action.type) {
        case SEARCH_WIDGET_UPDATE: {
            return {
                ...state,
                ...action.payload
            };
        }
    }

    return state;
}
import { ERROR_WIDGET_SERVER, ERROR_WIDGET_CLIENT, ERROR_WIDGET_RESET } from '../../constants';

let initialState = {
    messageError: null,
    stackTraceError: null,
    isError: false,
};
export default function SystemErrorReducer(state = initialState, action) {
    switch (action.type) {
        case ERROR_WIDGET_SERVER: {
            let error = {
                messageError: `(${action.payload.statusCode}) - ${action.payload.error}`,
                stackTraceError: action.payload.stackTrace,
                isError: true,
            };
            return {
                ...error
            }
        }

        case ERROR_WIDGET_CLIENT: {
            let error = {
                messageError: action.payload.messageError,
                isError: true,
            };
            return {
                ...error
            }
        }

        case ERROR_WIDGET_RESET: {
            return {
                ...initialState
            }
        }

        default: {
            return state;
        }
    }
}
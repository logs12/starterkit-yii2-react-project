import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router';
import { resetError } from './actions'

import ErrorComponent from './component';

const mapStateToProps = state => ({
    messageError: state.common.systemError.messageError,
    stackTraceError: state.common.systemError.stackTraceError,
});

const mapDispathToProps = dispatch => ({
    errorReset: bindActionCreators(resetError, dispatch),
});

@connect(mapStateToProps, mapDispathToProps)
export default class ErrorWidget extends Component {

    handleRedirectToMainPage() {
        this.props.errorReset();
    }

    render () {

        const { messageError , stackTraceError } = this.props;

        return (
            <ErrorComponent
                messageError={messageError}
                stackTraceError={stackTraceError}
                handleRedirectToMainPage={::this.handleRedirectToMainPage}
            />
        )
    }
}

import './style.scss';

import React, { Component }  from 'react';
import ClassNames from 'classnames';
import Lightbox from 'react-image-lightbox';

export default class GalleryLightboxWidget extends Component {

    state = {
        photoIndex: 0,
        isOpen: false
    };

    /**
     * Handle click image gallery
     * @param event
     * @param index
     */
    onClickImage(event, index) {
        event.preventDefault();

        this.setState({
            isOpen: true,
            photoIndex: index,
        });
    }

    /**
     * Render Gallery component
     * @param images
     * @returns {XML}
     */
    renderGallery(images) {

        let imgContainer = images.map((file, index) => {
            return <img key={index} src={file.path}  onClick={(event) => this.onClickImage(event, index)} />;
        });
        return (
            <div className="gallery">
                {imgContainer}
            </div>
        );
    }

    /**
     * Render Lightbox component
     * @param images
     * @returns {*}
     */
    renderLightbox(images) {

        const {
            photoIndex,
            isOpen,
        } = this.state;

        let imagesSrc = [];
        if (isOpen && images.length) {

            images.forEach((file) => {
                imagesSrc.push(file.path);
            });

            return <Lightbox
                mainSrc={imagesSrc[photoIndex]}
                nextSrc={imagesSrc[(photoIndex + 1) % imagesSrc.length]}
                prevSrc={imagesSrc[(photoIndex + imagesSrc.length - 1) % imagesSrc.length]}

                onCloseRequest={() => this.setState({ isOpen: false })}
                onMovePrevRequest={() => this.setState({
                    photoIndex: (photoIndex + imagesSrc.length - 1) % imagesSrc.length,
                })}
                onMoveNextRequest={() => this.setState({
                    photoIndex: (photoIndex + 1) % imagesSrc.length,
                })}
            />
        }
        return null;
    }

    render() {

        const { className, images } = this.props;

        const classNames = ClassNames('gallery-lightbox-widget', className);

        return (
            <div className={classNames}>
                {this.renderGallery(images)}
                {this.renderLightbox(images)}
            </div>
        );
    }
}
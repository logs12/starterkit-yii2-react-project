import {
    FORM_WIDGET_INIT,
    FORM_WIDGET_RESET,
} from '../../constants';
import ActionsFactory from '../../services/ActionsFactory';

/**
 * Первоначальная инициализация формы в глобальном state
 * @param formName
 * @returns {function(*): *}
 */
export function initActionFormWidget(formName) {
    return dispatch => dispatch({
        type: FORM_WIDGET_INIT,
        payload: {
            formName: formName,
        },
    });
}

/**
 * Actions submit form
 * @param actionName {string} - название action который обрабатывает submit
 * @param data {object} - объект с данными формы
 * data{
 *  formName {string} - название формы
 *  data {object} - объект с данными формы
 *  url {string} - url на который происходит отправка данных
 * }
 * @returns {{types: *[], promise: (function())}}
 */
export function submitActionFormWidget(actionName, data) {
    return ActionsFactory(actionName)( data, {formName: data.formName});
}

export function reset(formName, modelForm, modelFormError) {

    return dispatch => dispatch({
        type: FORM_WIDGET_RESET,
        formName,
        modelForm,
        modelFormError,
    });
}
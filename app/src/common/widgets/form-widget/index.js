import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';
import FormWidgetHoc from './container';

/**
 * @param {string} actionName - action submit form
 * @param {string} entityName - name model for bindings model to child widget
 * @param {object} initModel - object model with data for first init form for update entity
 * @param {string} url - url request
 * @param {string} formName - name form for
 * @param {array} attributes - object attributes with data
 *
 <FormWidget
        actionName={this.formAction}
        entityName="Building"
        initModel={Building}
        attributes={[
            'state',
            'city',
            'address_one',
            'address_two',
            'photos',
        ]}
        url={BUILDING_URL_REQUEST(buildingId)}
        formName={'building'}
    >
        Child widget
    </FormWidget>
*/

export default class FormWidget extends Component {

    /**
     * @param {string} actionName - action submit form
     * @param {string} entityName - name entity for bindings model to child widget
     * @param {string} formName - name form for
     * @param {string} url - url request
     * @param {string} className - class name widget
     * @param {object} initModel - object model with data
     * @param {array} attributes - object attributes with data
     */
    static propTypes = {
        actionName: PropTypes.string,
        entityName: PropTypes.string.isRequired,
        formName: PropTypes.string,
        url: PropTypes.string.isRequired,
        className: PropTypes.string,
        initModel: PropTypes.object,
        attributes: PropTypes.array.isRequired,
    };

    /**
     * Инициализируем контроль типов свойств контекста
     * @type {{entityName: string}}
     */
    static childContextTypes = {
        entityNameForm: PropTypes.string,
        formName: PropTypes.string,
    };

    getChildContext() {
        return {
            entityNameForm: this.props.entityName,
            formName: this.renderFormName(),
        };
    }

    /**
     * Устанавливаем свойства по дефолту
     * @type {{actionName: string, entityName: string, url: string}}
     */
    static defaultProps = {
        actionName: 'submitForm',
        url: '/',
        className: 'form-widget',
        initModel: null,
        formName: null,
    };

    renderFormName() {
        if (!this.props.formName) {
            return `${this.props.entityName}Form`;
        }
        return this.props.formName;
    }

    render () {
        const {
            actionName,
            url,
            className,
            initModel,
            children,
            entityName,
            attributes,
        } = this.props;

        const formName = this.renderFormName();

        return  createElement(FormWidgetHoc(
            actionName,
            url,
            className,
            initModel,
            children,
            formName,
            entityName,
            attributes,
        ))
    }
}
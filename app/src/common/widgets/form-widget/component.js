import React from 'react';
import PropTypes from 'prop-types';

const Form = props => {
    return (
        <form onSubmit={props.submitHandle} className={props.className}>
            {props.children}
        </form>
    )
};

Form.propTypes = {
    submitHandle: PropTypes.func.isRequired,
};

export default Form;
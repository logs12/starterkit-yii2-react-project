import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import SnackbarComponent from './component';
import ObfuscatorWidget from '../obfuscator-widget';
import * as actions from './actions';

const mapStateToProps = state => ({
    snackbarWidget: state.common.snackbarWidget,
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
});

@connect(mapStateToProps, mapDispatchToProps)

export default class SnackbarWidget extends Component {

    state = {
        isSnackbarActive: false,
    };

    componentWillMount() {
        this.setState({ isSnackbarActive: this.props.snackbarWidget.isSnackbarActive });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ isSnackbarActive: nextProps.snackbarWidget.isSnackbarActive });
    }

    /**
     * Handle event time out snackbar
     */
    handleTimeoutSnackbar() {
        this.props.snackbarWidget.actionOne();
        this.props.actions.SnackbarWidgetInactiveAction();
    }

    /**
     * Handle event action one snackbar
     */
    handleClickActionOneSnackbar() {

        this.props.snackbarWidget.actionOne();
        if (this.props.snackbarWidget.actionOne) {
            this.props.snackbarWidget.actionOne();
        }

        this.props.actions.SnackbarWidgetInactiveAction();
    }

    /**
     * Handle event action two snackbar
     */
    handleClickActionTwoSnackbar() {

        if (this.props.snackbarWidget.actionTwo) {
            this.props.snackbarWidget.actionTwo();
        }

        this.props.actions.SnackbarWidgetInactiveAction();
    }

    handleClickObfuscator() {

        this.setState({ isSnackbarActive: false });
    }

    render() {

        const { isSnackbarActive } = this.state;
        const { messageSnackbar, actionOneTitle, actionTwoTitle, timeout, isTimeout } = this.props.snackbarWidget;

        return (
            <div className="snackbar-widget">
                <ObfuscatorWidget isVisible={isSnackbarActive} handleClickObfuscator={::this.handleClickObfuscator} />
                <SnackbarComponent
                    timeout={timeout}
                    active={isSnackbarActive}
                    onClickActionOne={::this.handleClickActionOneSnackbar}
                    onClickActionTwo={::this.handleClickActionTwoSnackbar}
                    onTimeout={::this.handleTimeoutSnackbar}
                    isTimeout={isTimeout}
                    actionOneTitle={actionOneTitle}
                    actionTwoTitle={actionTwoTitle}>
                        {messageSnackbar}
                </SnackbarComponent>
            </div>
        );
    }
}
import {
    SNACKBAR_WIDGET_ACTIVE,
    SNACKBAR_WIDGET_INACTIVE,
} from '../../constants';

let initialState = {
    timeout: 1500,
    isSnackbarActive: false,
    messageSnackbar: null,
    actionOneTitle: 'Undo',
    actionTwoTitle: '',
};

export default function SnackbarWidgetReducer(state = initialState, action) {
    switch (action.type) {
        case SNACKBAR_WIDGET_ACTIVE: {
            return {
                ...state,
                ...action.payload,
                ...{ isSnackbarActive: true },
            }
        }
        case SNACKBAR_WIDGET_INACTIVE: {
            return {
                ...state,
                ...initialState
            }
        }
    }

    return state;
}
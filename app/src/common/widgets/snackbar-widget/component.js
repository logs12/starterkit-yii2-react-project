import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// This component doesn't use the javascript from MDL.
// This is the expected behavior and the reason is because it's not written in
// a way to make it easy to use with React.
const ANIMATION_LENGTH = 250;

const propTypes = {
    action: PropTypes.string,
    active: PropTypes.bool.isRequired,
    className: PropTypes.string,
    onClickActionOne: PropTypes.func,
    onClickActionTwo: PropTypes.func,
    onTimeout: PropTypes.func,
    timeout: PropTypes.number,
    isTimeout: PropTypes.bool,
};

const defaultProps = {
    timeout: 2750,
    isTimeout: false,
    textColorButtonOne: 'red',
    textColorButtonTwo: 'greenyellow',
};

class Snackbar extends Component {

    timeoutId = null;
    clearTimeoutId = null;
    active = false;
    onActionOneClick = null;
    onActionTwoClick = null;

    state = {
        open: this.active
    };

    constructor(props) {
        super(props);
        this.clearTimer = this.clearTimer.bind(this);
    }

    componentWillMount() {
        this.active = this.props.active;
        this.onActionOneClick = this.props.onClickActionOne;
        this.onActionTwoClick = this.props.onClickActionTwo;
    }

    componentWillReceiveProps(nextProps) {
        this.active = nextProps.active;
        this.onActionOneClick = nextProps.onClickActionOne;
        this.onActionTwoClick = nextProps.onClickActionTwo;
        this.setState({
            open: this.active
        });
    }

    /**
     *
     */
    componentDidUpdate() {

        if (this.props.isTimeout) {
            if (this.timeoutId) {
                clearTimeout(this.timeoutId);
            }

            if (this.props.active) {
                this.timeoutId = setTimeout(this.clearTimer, this.props.timeout);
            }
        }
    }

    /**
     * При демонтировании компонента из DOM отключаем таймеры
     */
    componentWillUnmount() {

        if (this.props.isTimeout) {
            if (this.timeoutId) {
                clearTimeout(this.timeoutId);
                this.timeoutId = null;
            }
            if (this.clearTimeoutId) {
                clearTimeout(this.clearTimeoutId);
                this.clearTimeoutId = null;
            }
        }
    }

    clearTimer() {
        this.timeoutId = null;
        this.setState({ open: false });

        this.clearTimeoutId = setTimeout(() => {
            this.clearTimeoutId = null;
            this.props.onTimeout();
        }, ANIMATION_LENGTH);
    }

    /**
     * Action обработчик первой кнопки
     */
    actionOneClick() {
        this.onActionOneClick();
        this.setState({ open: this.active });
    }

    /**
     * Action обработчик второй кнопки
     */
    actionTwoClick() {
        this.onActionTwoClick();
        this.setState({ open: this.active });
    }

    render() {

        const {
            actionOneTitle,
            actionTwoTitle,
            className,
            children,
            textColorButtonOne,
            textColorButtonTwo,
            ...otherProps
        } = this.props;

        const { open } = this.state;

        const classes = classNames('mdl-snackbar', {
            'mdl-snackbar--active': open
        }, className);

        delete otherProps.onTimeout;
        delete otherProps.timeout;

        return (
            <div className={classes} aria-hidden={!open}>
                <div className="mdl-snackbar__text">{this.active && children}</div>
                {this.active && actionOneTitle && <button className="mdl-snackbar__action"
                                                          style={{color: textColorButtonOne}}
                                                          type="button"
                                                          onClick={::this.actionOneClick}>{actionOneTitle}</button>}
                {this.active && actionTwoTitle && <button className="mdl-snackbar__action"
                                                          style={{color: textColorButtonTwo}}
                                                          type="button"
                                                          onClick={::this.actionTwoClick}>{actionTwoTitle}</button>}
            </div>
        );
    }
}

Snackbar.propTypes = propTypes;
Snackbar.defaultProps = defaultProps;

export default Snackbar;

import './style.scss';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from "react-router-redux";

import PaginationComponent from './component';

/**
 *
 * @param options
 * @constructor
 */
export default function PaginationWidget(options) {

    let entityName = null,
        url = null;

    if (options.hasOwnProperty('entityName')) {
        entityName = options.entityName;
    } else {
        new Error('Not found parameters "entityName" in PaginationWidget');
    }

    if (options.hasOwnProperty('url')) {
        url = options.url;
    } else {
        new Error('Not found parameters "url" in PaginationWidget');
    }

    const mapStateToProps = (state) => {
        return ({
            entityName: entityName,
            collection: state[entityName].collection,
            pagination: state[entityName].pagination,
            url: url,
        });
    };

    const mapDispatchToProps = dispatch => {
        return ({
            push: bindActionCreators(push, dispatch),
        });
    };

    return connect(mapStateToProps, mapDispatchToProps)(PaginationComponent);
}
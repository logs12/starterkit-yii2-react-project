import './style.scss';

import React, { Component } from 'react';

import Icon from 'react-mdl/lib/Icon';
import Button from 'react-mdl/lib/Button';


export default class PaginationComponent extends Component {

    current = 0;
    pagerButtonPrevious = '';
    pagerButtonNextState = '';

    state = {
        startValue: 0,
        endValue: 0,
    };

    componentWillReceiveProps(nextProps) {
        this.current = parseInt(nextProps.pagination.current);
        this.calculationPaging(nextProps.pagination);
    }

    handleArrowLeft() {
        this.current = --this.current;
        if (this.current === 1) {
            this.props.push(`${this.props.url}`);
        } else {
            this.props.push(`${this.props.url}?page=${this.current}`);
        }
    }

    handleArrowRight() {
        this.current = ++this.current;
        this.props.push(`${this.props.url}?page=${this.current}`);
    }

    calculationPaging(pagination) {

        // Расчет начального состояния
        this.setState({
            startValue: (pagination.perPage * (this.current - 1)) + 1,
        });

        // Расчёт конечного значения
        this.setState({
            endValue: (pagination.perPage * this.current) > pagination.total ? pagination.total : pagination.perPage * this.current,
        });

        // Кнопка перехода на предыдущую страницу
        if (this.current <= 1) {
            this.pagerButtonPrevious = 'disabled';
        } else {
            this.pagerButtonPrevious = '';
        }

        // Кнопка перехода на следующую страницу
        let maxPage = Math.ceil(pagination.total / pagination.perPage);
        if (this.current >= maxPage) {
            this.pagerButtonNextState = 'disabled';
        } else {
            this.pagerButtonNextState = '';
        }

        // Отключаем обе кнопки если общее количество записей меньше числа показываемых записей
        if (Number(pagination.perPage) >= Number(pagination.total)) {
            this.pagerButtonPrevious = 'disabled';
            this.pagerButtonNextState = 'disabled';
        }
    }

    render() {

        const { startValue, endValue } = this.state;
        const { pagination, entityName } = this.props;
        const {perPage, total} = pagination;

        return(
            <div className="pagination-widget">
                <div className="mdl-layout-spacer"></div>
                <div className="pagination-widget__pager">
                    <span>{entityName} per page:</span>
                    <span className="pagination-widget__pager-per-page">{perPage}</span>
                    <span className="pagination-widget__pager-start">{startValue}</span>
                    <span> - </span>
                    <span className="pagination-widget__pager-end">{endValue}</span>
                    <span className="pagination-widget__pager-text widget-table__pager-text--narrow">of</span>
                    <span className="pagination-widget__pager-total">{total}</span>
                    <span className="pagination-widget__pager-navigation">
                    <Button raised
                            className="pagination-widget__pager-previous" onClick={::this.handleArrowLeft}
                            disabled={this.pagerButtonPrevious}>
                        <Icon name="keyboard_arrow_left"/>
                    </Button>
                    <Button raised
                            className="pagination-widget__pager-next" onClick={::this.handleArrowRight}
                            disabled={this.pagerButtonNextState}>
                        <Icon name="keyboard_arrow_right"/>
                    </Button>
                </span>
                </div>
            </div>
        )
    }
}

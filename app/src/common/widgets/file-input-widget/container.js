import React, { Component, PropTypes } from 'react';
import FileInputComponent from "./component";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';

import { updateInputText }  from './actions';

import { WIDGET_INPUT_TEXT_ACTION_NAME } from '../../constants';

const mapStateToProps = state => {
    return ({
        formWidget: state.common.formWidget
    });
};

const mapDispatchToProps = dispatch => {
    return {
        updateInputText: bindActionCreators(updateInputText, dispatch)
    }
};
/**
 * Подключение к reduxStore
 */
@connect(mapStateToProps, mapDispatchToProps)

/**
 * Виджет можно использовать независимо от формы, но в таком случае необходимо явно прописывать название reducer
 * в глобальном state в свойстве reducerName, которому принадлежат данные
    <InputText
        name = 'password'
        placeholder = 'Пароль'
        actionName = 'updateInputText'
        urlSubmit = ''
        reducerName = 'entity'
    />
 */
export default class FileInputWidget extends Component {

    /**
     * Инициализируем контроль типов свойств
     * @type {{name: *, placeholder: *, label: *}}
     */
    static propTypes = {
        name: PropTypes.string.isRequired,
        placeholder: PropTypes.string,
        actionName: PropTypes.string,
        urlSubmit: PropTypes.string,
        className: PropTypes.string,
    };


    /**
     * @param reducerName - название reducer
     * @type {{actionName: string}}
     */
    static defaultProps = {
        reducerName: 'formWidget',
        actionName: WIDGET_INPUT_TEXT_ACTION_NAME,
        urlSubmit: '/',
        className: 'input-text-widget',
    };

    /**
     * Инициализируем контроль типов свойств контекста
     * @type {{url: *}}
     */
    static contextTypes = {
        formName: PropTypes.string.isRequired,
    };

    /**
     * Инициализируем состояния
     * @type {{errors: Array}}
     */
    state = {
        fileObject: {},
        errors: [],
    };

    files = [];

    componentWillMount() {

        this.files = this.props[this.props.reducerName][this.context.formName].values[this.props.name];
    }

    /**
     * Хук на получение новых свойств
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {

        this.error = nextProps[this.props.reducerName][this.context.formName].errors[this.props.name];
        this.files = nextProps[this.props.reducerName][this.context.formName].values[this.props.name];
    }

    getFileData(fileObject, readerResult) {
        return {
            name: fileObject.name,
            size: fileObject.size,
            type: fileObject.type,
            base64: readerResult.split(',')[1],
        };
    };

    /**
     * Обработчик события изменения input, отправка введеного значения в store
     */
    onChange(event) {

        event.preventDefault();

        const fileObject = event.target.files[0];

        let reader  = new FileReader();

        reader.addEventListener("load", () => {
            this.setState({
                fileObject: fileObject,
            });

            // Передаем в редьюсер значение поля после завершения чтения файла
            this.props.updateInputText(
                this.props.actionName,
                {
                    formName: this.context.formName,
                    inputName: this.props.name,
                    value: this.getFileData(fileObject, reader.result)
                },
            );

        }, false);

        reader.addEventListener("error", () => {
            debugger;
        }, false);

        //this.setTitle(this.getSpinner());

        reader.readAsDataURL(fileObject);
    }

    render () {

        const { placeholder, className } = this.props;
        let { fileObject } = this.state;

        return (
            <div className="widget-file-input">
                <FileInputComponent
                    onChange={::this.onChange}
                    error={this.error}
                    title={placeholder}
                    className="widget-file-input__container"
                    files={this.files}
                />
            </div>
        )
    }

}


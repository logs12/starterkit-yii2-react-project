import "./style/style.scss";

import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import classNames from 'classnames';
import mdlUpgrade from 'react-mdl/lib/utils/mdlUpgrade';
import IconButton from 'react-mdl/lib/IconButton';

const propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool,
    error: PropTypes.node,
    id: PropTypes.string,
    inputClassName: PropTypes.string,
    title: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};

class FileInputComponent extends Component {

    componentDidMount() {
        if (this.props.error) {
            this.setAsInvalid();
        }
    }

    setAsInvalid() {
        const elt = findDOMNode(this);
        if (elt.className.indexOf('is-invalid') < 0) {
            elt.className = classNames(elt.className, 'is-invalid');
        }
    }

    renderFileContainer(files) {

        return files.map((file) => {
            return <div className="mdl-card">
                <IconButton name="clear"/>
                <img src={file.path} />
                <span>{file.filename}.{file.extension}</span>
            </div>
        });
    }

    render() {
        const { className, inputClassName, buttonClassName, id,
            error, title, fileObject, onChange } = this.props;

        let fileName  = fileObject.name;

        const customId = id || `textfield-${title.replace(/[^a-z0-9]/gi, '')}`;

        const errorContainer = !!error && <span className="mdl-textfield__error">{error}</span>;

        const containerClasses = classNames(className);
        const inputClassNames = classNames("widget-file-input__input",inputClassName);
        const buttonClasses = classNames('mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary mdl-button--file', buttonClassName);

        return  (
            <div className={containerClasses}>

                <button className={buttonClasses} type="button">
                    {title}
                    <input id={customId} className={inputClassNames} type="file" onChange={onChange} />
                </button>

                <div className="widget-file-input__title">{fileName}</div>
                {errorContainer}

                {errorContainer}
            </div>
        );
    }
}

FileInputComponent.propTypes = propTypes;

export default mdlUpgrade(FileInputComponent);


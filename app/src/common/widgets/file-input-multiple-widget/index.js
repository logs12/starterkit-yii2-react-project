import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';
import FileInputMultipleWidgetContainerHoc from './container';

/**
 * Виджет можно использовать независимо от формы, но в таком случае необходимо явно прописывать название reducer
 * в глобальном state в свойстве reducerName, которому принадлежат данные
    <InputText
        name = 'password'
        placeholder = 'Пароль'
        actionName = 'updateInputText'
        urlSubmit = ''
        reducerName = 'entity'
    />
 */
export default class FileInputMultipleWidget extends Component {

    /**
     * Инициализируем контроль типов свойств
     * @type {{name: *, placeholder: *, label: *}}
     */
    static propTypes = {
        name: PropTypes.string.isRequired,
        title: PropTypes.string,
        actionName: PropTypes.string,
        urlSubmit: PropTypes.string,
        entityName: PropTypes.string,
    };


    /**
     * @param reducerName - название reducer
     * @type {{actionName: string}}
     */
    static defaultProps = {
        type: 'file',
        urlSubmit: '/',
        className: null,
        title: 'Upload',
    };

    /**
     * Инициализируем контроль типов свойств контекста
     * @type {{url: *}}
     */
    static contextTypes = {
        entityNameForm: PropTypes.string,
    };

    render () {
        const {
            name,
            title,
            actionName,
            urlSubmit,
        } = this.props;

        const { entityNameForm } = this.context;

        let entityName = this.props.entityName ? this.props.entityName : entityNameForm;

        return createElement(FileInputMultipleWidgetContainerHoc(
            name,
            title,
            actionName,
            urlSubmit,
            entityName,
        ));
    }
}
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as _ from 'lodash';

import FileInputMultipleComponent from "./component";

import { actionFileInputMultiple }  from './actions';

import {
    FILE_INPUT_MULTIPLE_WIDGET_CONVERT_TO_BASE64,
    FILE_INPUT_MULTIPLE_WIDGET_UPDATE_VALUE,
    FILE_INPUT_MULTIPLE_WIDGET_DELETE_VALUE,
} from '../../constants';

/**
 * Виджет можно использовать независимо от формы, но в таком случае необходимо явно прописывать название reducer
 * в глобальном state в свойстве reducerName, которому принадлежат данные
    <InputText
        name = 'password'
        title = 'Пароль'
        actionName = 'updateInputText'
        urlSubmit = ''
        reducerName = 'entity'
    />
 */
export default function FileInputMultipleWidgetContainerHoc(
    name,
    title,
    actionName,
    urlSubmit,
    entityName,
) {

    const mapStateToProps = state => {
        return ({
            name:name,
            title: title,
            actionName: actionName,
            urlSubmit: urlSubmit,
            model: state[entityName].model,
        });
    };

    const mapDispatchToProps = dispatch => {
        return {
            actionFileInputMultiple: bindActionCreators(actionFileInputMultiple, dispatch),
        }
    };
    /**
     * Подключение к reduxStore
     */
    @connect(mapStateToProps, mapDispatchToProps)

    class FileInputMultipleWidgetContainer extends Component {
        /**
         * Инициализируем контроль типов свойств
         * @type {{name: *, title: *}}
         */
        static propTypes = {
            name: PropTypes.string.isRequired,
            title: PropTypes.string,
            actionName: PropTypes.string,
            urlSubmit: PropTypes.string,
            className: PropTypes.string,
        };


        /**
         * @param reducerName - название reducer
         * @type {{actionName: string}}
         */
        static defaultProps = {
            type: 'file',
            reducerName: 'formWidget',
            urlSubmit: '/',
            className: null,
            fileInputMultipleWidgetConvertToBase64Constant: `${entityName.toUpperCase()}_${FILE_INPUT_MULTIPLE_WIDGET_CONVERT_TO_BASE64}`,
            fileInputMultipleWidgetUpdateValueConstant: `${entityName.toUpperCase()}_${FILE_INPUT_MULTIPLE_WIDGET_UPDATE_VALUE}`,
            fileInputMultipleWidgetDeleteValueConstant: `${entityName.toUpperCase()}_${FILE_INPUT_MULTIPLE_WIDGET_DELETE_VALUE}`,
        };

        /**
         * Инициализируем контроль типов свойств контекста
         * @type {{url: *}}
         */
        static contextTypes = {
            entityNameForm: PropTypes.string,
            formName: PropTypes.string.isRequired,
        };

        /**
         * Инициализируем состояния
         * @type {{isActiveDropZone: boolean, files: Array, errors: Array}}
         */
        state = {
            isActiveDropZone: false,
            files: [],
            errors: [],
        };

        componentWillMount() {
            let files = this.props.model.values[this.props.name];
            this.convertFileToBase64(files);
        }

        /**
         * Хук на получение новых свойств
         * @param nextProps
         */
        componentWillReceiveProps(nextProps) {

            this.error = nextProps.model.errors[this.props.name];

            let files = nextProps.model.values[this.props.name];

            this.convertFileToBase64(files);
        }

        /**
         *  Convert already uploaded files to base64
         *  @param files {collection} - collection uploading files
         */
        convertFileToBase64(files) {

            if (_.isArray(files)) {

                let filesForConvertBase64 = [];
                files.forEach((file) => {
                    if (file) {
                        if (file.hasOwnProperty('path')) {
                            filesForConvertBase64.push(file);
                        }
                    }
                });

                if (filesForConvertBase64.length > 0) {

                    let filesArrayPromises = files.map((file) => {
                        return new Promise((resolve, reject) => {
                            fetch(file.path)
                                .then((res) => {
                                    res.blob().then((blob) => {

                                        let fileObject = {
                                            name: `${file.filename}.${file.extension}`,
                                            size: blob.size,
                                            type: blob.type,
                                        };

                                        this.readFile(blob, (readerResult) => {
                                            resolve(this.getFileData(fileObject, readerResult));
                                        });
                                    })
                                });
                        });
                    });

                    Promise.all(filesArrayPromises).then((files) => {
                        // Передаем в редьюсер значение поля
                        this.props.actionFileInputMultiple(
                            this.props.fileInputMultipleWidgetConvertToBase64Constant,
                            {
                                [this.props.name]: files,
                            },
                        );
                    });
                } else {
                    this.setState({
                        files: files
                    });
                }
            }
        }

        /**
         * Get information about uploading file
         * @param fileObject
         * @param readerResult
         * @returns {{name: string, size: *, type: *, base64: *}}
         */
        getFileData(fileObject, readerResult) {
            return {
                name: fileObject.name,
                size: fileObject.size,
                type: fileObject.type,
                base64: readerResult.split(',')[1],
            };
        };

        /**
         * Read files on load
         * @param fileObject - object with information about uploading files
         * @param onLoadCallback {function} - callback function on load files
         */
        readFile(fileObject, onLoadCallback) {

            let reader = new FileReader();

            reader.addEventListener("load", () => {

                onLoadCallback(reader.result);

            }, false);

            reader.addEventListener("error", () => {
                debugger;
            }, false);

            reader.readAsDataURL(fileObject);
        }


        /**
         * Send files in store redux
         * @param filesObject
         */
        sendFileInStore(filesObject) {
            for (let i = 0; i < filesObject.length; i++) {
                let fileObject = filesObject[i];
                this.readFile(fileObject, (readerResult) => {
                    // Передаем в редьюсер значение поля после завершения чтения файла
                    // Передаем в редьюсер значение поля
                    this.props.actionFileInputMultiple(
                        this.props.fileInputMultipleWidgetUpdateValueConstant,
                        {
                            [this.props.name]: this.getFileData(fileObject, readerResult),
                        },
                    );
                });
            }
        }


        /**
         * Обработчик события изменения input, отправка введеного значения в store
         * @param event
         */
        handleChange(event) {

            event.preventDefault();
            const filesObject = event.target.files;
            this.sendFileInStore(filesObject);

        }

        /**
         * Handler file deleted
         * @param event
         * @param deleteIdFile
         */
        handleDeleteFile(event, deleteIdFile) {

            event.preventDefault();

            // Делаем копию массива, для иммутабельности
            let files = this.state.files.slice();

            files.splice(deleteIdFile, 1);

            this.props.actionFileInputMultiple(
                this.props.fileInputMultipleWidgetDeleteValueConstant,
                {
                    [this.props.name]: files,
                },
            );
        }

        /** Drag&Drop **/

        onDrop(event) {

            event.preventDefault();
            let filesObject = event.dataTransfer ? event.dataTransfer.files : event.target.files;
            this.sendFileInStore(filesObject);
            this.onDragLeave(event);

            this.setState({
                isActiveDropZone: false
            });
        }

        onDragOver(event) {
            event.preventDefault();
            event.stopPropagation();
        }

        onDragEnter() {
            this.setState({
                isActiveDropZone: true
            });
        }

        onDragLeave() {
            this.setState({
                isActiveDropZone: false
            });
        }

        render() {

            const {title, className} = this.props;
            let {files, isActiveDropZone} = this.state;

            return (
                <FileInputMultipleComponent
                    className={className}
                    isActiveDropZone={isActiveDropZone}
                    handleChange={::this.handleChange}
                    error={this.error}
                    title={title}
                    files={files}
                    handleDeleteFile={::this.handleDeleteFile}
                    onDrop={::this.onDrop}
                    onDragOver={::this.onDragOver}
                    onDragEnter={::this.onDragEnter}
                    onDragLeave={::this.onDragLeave}
                />
            )
        }
    }


    return FileInputMultipleWidgetContainer;
}


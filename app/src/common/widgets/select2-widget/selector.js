import { createSelector } from 'reselect';
import _ from 'lodash';

/**
 * Selected Collection Building
 * @param state
 * @returns {*}
 */
const collectionSelector = (collection) => {
    debugger;
    return state.Building.collection;
};

/**
 * id for filtering collection
 * @param state
 * @param selectedBuildingId
 */
const selectedBuildingSelector = (state, selectedBuildingId) => (Number(selectedBuildingId));

/**
 * Filtering collection
 * @param buildings
 * @param selectedBuildingId
 * @returns {*|{}|T}
 */
const getBuildingSelector = (buildings, selectedBuildingId) => {
    return _.find(buildings, {id: selectedBuildingId});
};


export const getModel = createSelector(
    collectionSelector,
    selectedBuildingSelector,
    getBuildingSelector,
);

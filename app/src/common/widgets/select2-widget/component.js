import './style/style.scss'

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import classNames from 'classnames';
import mdlUpgrade from 'react-mdl/lib/utils/mdlUpgrade';

const propTypes = {
    id: PropTypes.string,
    label: PropTypes.string.isRequired,
    floatingLabel: PropTypes.bool,
    disabled: PropTypes.bool,
    error: PropTypes.node,
    pattern: PropTypes.string,
    inputValue: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    onChangeInput: PropTypes.func,
    onFocusInput: PropTypes.func,
    onBlurInput: PropTypes.func,
    handlerClickClearInput: PropTypes.func,
    relationFieldName: PropTypes.string,
    relationCollection: PropTypes.array,
};

class Select2WidgetComponent extends Component {

    componentDidMount() {
        if (this.props.error && !this.props.pattern) {
            this.setAsInvalid();
        }
    }

    componentDidUpdate(prevProps) {

        if (this.props.disabled !== prevProps.disabled) {
            this.refs.containerInput.MaterialTextfield.checkDisabled();
        }
        if (this.props.inputValue !== prevProps.inputValue && this.inputRef !== document.activeElement) {
            this.refs.containerInput.MaterialTextfield.change(this.props.inputValue);
        }
        if (this.props.error && !this.props.pattern) {
            // Every time the input gets updated by MDL (checkValidity() or change())
            // its invalid class gets reset. We have to put it again if the input is specifically set as "invalid"
            this.setAsInvalid();
        }
    }

    /**
     * Set input class error
     */
    setAsInvalid() {
        const elt = findDOMNode(this);
        if (elt.className.indexOf('is-invalid') < 0) {
            elt.className = classNames(elt.className, 'is-invalid');
        }
    }

    render() {
        const {
            id,
            error,
            inputValue,
            floatingLabel,
            label,
            children,
            onChangeInput,
            handlerClickClearInput,
            onFocusInput,
            onBlurInput,
        } = this.props;

        const customId = id || `textfield-${label.replace(/[^a-z0-9]/gi, '')}`;

        const input = <input id={customId}
                             className={'mdl-textfield__input'}
                             value={inputValue}
                             onChange={onChangeInput}
                             onFocus={onFocusInput}
                             onBlur={onBlurInput}
                             ref={(c) => (this.inputRef = c)}
                             autoComplete="off"
                />;

        const labelContainer = <label className="mdl-textfield__label" htmlFor={customId}>{label}</label>;
        const buttonCloseContainer = <i className="widget-select2__clear material-icons" onClick={handlerClickClearInput}>close</i>;
        const errorContainer = !!error && <span className="mdl-textfield__error">{error}</span>;

        const containerClasses = classNames('widget-select2__input-container mdl-textfield mdl-js-textfield', {
            'mdl-textfield--floating-label': floatingLabel,
        });

        return (
            <div className={containerClasses} ref='containerInput'>
                {input}
                {labelContainer}
                {buttonCloseContainer}
                {errorContainer}
                {children}
            </div>
        );
    }
}

Select2WidgetComponent.propTypes = propTypes;

export default mdlUpgrade(Select2WidgetComponent);
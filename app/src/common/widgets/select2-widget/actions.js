import {
    INIT_SELECT2_WIDGET,
    ACTION_NAME_SELECT2_WIDGET,
    UPDATE_VALUE_SELECT2_WIDGET,
    SHOW_DROPDOWN_SELECT2_WIDGET,
} from './constants';
import ActionsFactory from '../../services/ActionsFactory';


/**
 *  Action init Select2Widget
 * @param nameSelect2Widget
 * @returns {{type, nameSelect2Widget: *}}
 */
export function initSelect2Widget(nameSelect2Widget, value) {
    return {
        type: INIT_SELECT2_WIDGET,
        nameSelect2Widget: nameSelect2Widget,
        value: value,
    }
}

/**
 *
 * @param actionName
 * @param nameSelect2Widget
 * @param value
 * @returns {*}
 */
export function changeInputSelect2Widget(actionName, nameSelect2Widget, value) {
    try {

        // Если actionName не равен дефолтному, то подключаем его из фабрики action
        if (actionName !== ACTION_NAME_SELECT2_WIDGET) {
            return ActionsFactory(actionName)(nameSelect2Widget, value);
        }

        if (nameSelect2Widget) {
            new Error(`В ${actionName} select2Widget не передано название виджета`);
        }
        if (value) new Error(`В ${actionName} select2Widget не передано значение input`);

        return {
            type: UPDATE_VALUE_SELECT2_WIDGET,
            nameSelect2Widget: nameSelect2Widget,
            value: value,
        }
    } catch(error) {
        alert(error);
    }
}
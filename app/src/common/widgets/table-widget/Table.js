import React, { PropTypes, Component, createElement } from 'react';
import classNames from 'classnames';
import clamp from 'clamp';
import shadows from 'react-mdl/lib/utils/shadows';
import TableHeader from './TableHeader';
import TableActionsHeader from './TableActionsHeader';
import PaginationWidget from '../pagination-widget/container';
import makeSelectable from './Selectable';
import makeSortable from './Sortable';
import TableRow from './TableRow';
import TableBody from './TableBody';

const propTypes = {
    className: PropTypes.string,
    columns: (props, propName, componentName) => (
        props[propName] && new Error(`${componentName}: \`${propName}\` is deprecated, please use the component \`TableHeader\` instead.`)
    ),
    data: (props, propName, componentName) => (
        props[propName] && new Error(`${componentName}: \`${propName}\` is deprecated, please use \`rows\` instead. \`${propName}\` will be removed in the next major release.`)
    ),
    rowKeyColumn: PropTypes.string,
    /*rows: PropTypes.arrayOf(
        PropTypes.object
    ).isRequired,*/
    shadow: PropTypes.number,
    actionsTableHeader: PropTypes.arrayOf(
        PropTypes.object
    ),
    rowMenuActions: PropTypes.object,
};

const defaultProps = {
    widgetOptions: {
        search: null,
        pagination: null,
    }
};

class Table extends Component {


    render() {
        const {actionsTableHeader, rowMenuActions, collectionName, actionName, className, columns, shadow, children,
            rowKeyColumn, data, widgetOptions, attributes, ...otherProps } = this.props;

        const hasShadow = typeof shadow !== 'undefined';
        const shadowLevel = clamp(shadow || 0, 0, shadows.length - 1);

        const classes = classNames('mdl-data-table', {
            [shadows[shadowLevel]]: hasShadow
        }, className);

        const columnChildren = !!children
            ? React.Children.toArray(children)
            : columns.map(column =>
                <TableHeader
                    key={column.name}
                    className={column.className}
                    name={column.name}
                    numeric={column.numeric}
                    tooltip={column.tooltip}
                >
                    {column.label}
                </TableHeader>
            );

        return (
            <div className="table-widget wide mdl-card mdl-shadow--2dp">
                    <TableActionsHeader actions={actionsTableHeader} searchOptions={widgetOptions.search} />
                    <div className="table-widget__table">
                        <table className={classes} {...otherProps}>
                            <thead>
                                <tr>
                                    {columnChildren}
                                    <th></th>
                                </tr>
                            </thead>
                                {createElement(TableBody(
                                    collectionName,
                                    actionName,
                                    columnChildren,
                                    rowKeyColumn,
                                    rowMenuActions,
                                    attributes,
                                ))}
                        </table>
                    </div>
                    <div className="table-widget__pagination">
                        { (widgetOptions.pagination) ? createElement(PaginationWidget(widgetOptions.pagination)) : null}
                    </div>
            </div>
        );
    }
}

Table.propTypes = propTypes;
Table.defaultProps = defaultProps;

export default makeSortable(makeSelectable(Table));
export const UndecoratedTable = Table;

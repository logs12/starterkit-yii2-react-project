import React, { Component } from 'react';
import { connect } from 'react-redux';
import TableRow from './TableRow';
import { bindActionCreators } from "redux";
import ActionsFactory from '../../services/ActionsFactory';

/**
 * HOC component for render table body
 * @param collectionsName
 * @param actionName
 * @param columnChildren
 * @param rowKeyColumn
 * @param rowMenuActions
 * @param attributes
 * @returns {TableBodyContainer}
 * @constructor
 */
export default function TableBody(collectionsName, actionName, columnChildren, rowKeyColumn, rowMenuActions, attributes) {

    const mapStateToProps = state => {
        return {
            collection: state[collectionsName].collection,
            searchValue: state[collectionsName].search.searchValue,
            columnChildren: columnChildren,
            rowKeyColumn: rowKeyColumn,
            rowMenuActions: rowMenuActions,
            condition: state.routing.locationBeforeTransitions.search,
            actionName: actionName,
            attributes:attributes,

        }
    };

    const mapDispatchToProps = dispatch => ({
        getCollection: bindActionCreators(ActionsFactory(actionName), dispatch),
    });

    @connect(mapStateToProps, mapDispatchToProps)
    class TableBodyContainer extends Component {


        componentWillMount() {
            this.props.getCollection({condition: this.props.condition});
        }

        componentWillReceiveProps(nextProps) {
            if (this.props.condition != nextProps.condition) {
                this.props.getCollection({condition: nextProps.condition});
            }
        }

        render() {

            const { collection, columnChildren, rowKeyColumn, rowMenuActions, searchValue, attributes } = this.props;

            return (
                <tbody>
                {
                    collection.map((model, idx) => {
                        return <TableRow
                            columnChildren={columnChildren}
                            rowKeyColumn={rowKeyColumn}
                            rowMenuActions={rowMenuActions}
                            model={model}
                            idx={idx}
                            key={idx}
                            searchValue={searchValue}
                            attributes={attributes}
                        />
                    })
                }
                </tbody>
            )
        }
    }

    return TableBodyContainer;
}


import React, { Component, PropTypes, createElement } from 'react';
import TextInputWidgetHoc from './container';

/**
 * Виджет можно использовать независимо от формы, но в таком случае необходимо явно прописывать название
 * entityName - название сущности со значением которой будет работатьь виджет
 * в глобальном state в свойстве reducerName, которому принадлежат данные
     <InputText
         name = {'password'}
         label = {'Пароль'}
         initFocused

         // Если отдельно от формы, то необходимо заполнить нижеперечисленные свойства
         urlSubmit = {/url-submit}
         actionName = {'updateModelAction'}
         entityName = {'Building'}
     />
 */
export default class TextInputWidget extends Component {

    static defaultProps = {
        entityName: null,
        initFocused: false,
    };

    /**
     * Инициализируем контроль типов свойств контекста
     * @type {{url: *}}
     */
    static contextTypes = {
        entityNameForm: PropTypes.string.isRequired,
    };

    render () {
        const {
            name,
            label,
            initFocused,
            actionName,
            urlSubmit,
        } = this.props;

        let entityName = null;

        if (this.context.entityNameForm) {
            entityName = this.context.entityNameForm;
        } else {
            entityName = this.props.entityName;
        }

        return  createElement(TextInputWidgetHoc(
            name,
            label,
            initFocused,
            entityName,
            actionName,
            urlSubmit,
        ))
    }
};
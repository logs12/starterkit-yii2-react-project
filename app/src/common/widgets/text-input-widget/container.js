import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputTextComponent from "./component";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';

import { updateModelAction } from '../../actions/ModelActions';

export default function TextInputWidgetHoc(
    name,
    label,
    initFocused,
    entityName,
    urlSubmit,
    actionName,
) {

    const mapStateToProps = state => ({
        name: name,
        label: label,
        initFocused: initFocused,
        urlSubmit: urlSubmit,
        entityName: entityName,
        model: state[entityName].model,
        actionName: actionName,
    });

    const mapDispatchToProps = dispatch => ({
        updateModelAction: bindActionCreators(updateModelAction, dispatch)
    });

    @connect(mapStateToProps, mapDispatchToProps)

    class TextInputWidgetContainer extends Component {

        /**
         * Инициализируем контроль типов свойств
         * @type {{name: *, label: *, label: *}}
         */
        static propTypes = {
            name: PropTypes.string.isRequired,
            label: PropTypes.string,
            urlSubmit: PropTypes.string,
            className: PropTypes.string,
        };


        /**
         * @param reducerName - название reducer
         * @type {{actionName: string}}
         */
        static defaultProps = {
            type: 'text',
            reducerName: 'formWidget',
            urlSubmit: '/',
            className: 'input-text-widget',
            initModelConstant: `${entityName.toUpperCase()}_MODEL_INIT`,
            updateModelConstant: `${entityName.toUpperCase()}_MODEL_UPDATE`,
        };

        /**
         * Инициализируем состояния
         * @type {{errors: Array}}
         */
        state = {
            errors: [],
            inputTextValue: null,
        };

        //inputTextValue = '';

        componentWillMount() {
            this.setState({
                inputTextValue: this.props.model.values[this.props.name],
            });
        }

        /**
         * Хук на получение новых свойств
         * @param nextProps
         */
        componentWillReceiveProps(nextProps) {
            this.setState({
                inputTextValue: nextProps.model.values[this.props.name],
            });

            this.error = nextProps.model.errors[this.props.name];
        }

        /**
         * Обработчик события изменения input, отправка введеного значения в store
         */
        onChange(event) {
            event.preventDefault();

            this.setState({
                inputTextValue: event.target.value,
            });

            // Передаем в редьюсер значение поля
            this.props.updateModelAction(
                this.props.updateModelConstant,
                {
                    [this.props.name]: event.target.value ? event.target.value : null,
                }
            );
        }

        render () {

            const { name, label, className, initFocused } = this.props;

            const { inputTextValue } = this.state;

            return (
                <InputTextComponent
                    name = {name}
                    label = {label}
                    onChange = {::this.onChange}
                    error={this.error}
                    className={className}
                    initFocused={initFocused}
                    //inputTextValue={(this.inputTextValue !== null) ? this.inputTextValue.toString() : ''}
                    inputTextValue={(inputTextValue !== null) ? inputTextValue.toString() : ''}
                />
            )
        }
    }

    return TextInputWidgetContainer;
}

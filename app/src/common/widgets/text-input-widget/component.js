import React, { Component } from "react";
import TextField from 'react-mdl/lib/Textfield';

export default class InputTextComponent extends Component {

    componentDidMount() {
        if (this.props.initFocused && this.props.inputTextValue == '') {
            this.inputContainer.childNodes[0].classList.add('is-focused');
            this.inputContainer.childNodes[0].childNodes[0].focus();
        }
    };

    render() {
        const { className, onChange, error, label, inputTextValue } = this.props;

        return (
            <div className={className}
                 ref={(inputContainer) => {this.inputContainer = inputContainer}}>
                <TextField
                    floatingLabel
                    onChange={onChange}
                    error={error}
                    label={label}
                    value={inputTextValue}
                />
            </div>
        );
    }
}

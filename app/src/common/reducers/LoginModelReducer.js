import {
    LOGIN_MODEL_INIT,
    LOGIN_MODEL_UPDATE,
    LOGIN_FORM_MODEL_ERROR,
} from '../constants/index';

import BaseModelReducer from './BaseModelReducer';

let initialState = {};

export default function LoginModelReducer(state = initialState, action) {

    switch (action.type) {

        case LOGIN_MODEL_INIT: {
            return BaseModelReducer.initModel(state, action);
        }

        case LOGIN_MODEL_UPDATE: {
            return BaseModelReducer.updateModel(state, action);
        }

        case LOGIN_FORM_MODEL_ERROR : {
            return BaseModelReducer.errorModel(state, action);
        }
    }
    
    return state;
}

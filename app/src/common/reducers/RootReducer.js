import { combineReducers } from 'redux';
import FormWidgetReducer from '../widgets/form-widget/reducers';
import SystemErrorReducer from '../widgets/error-widget/reducers';
import ConfigDataReducer from './ConfigDataReducer';
import ProgressBarWidgetReducer from '../widgets/progress-bar-widget/reducer';
import SnackbarWidgetReducer from '../widgets/snackbar-widget/reducer';
import PaginationWidgetReducer from '../../common/widgets/pagination-widget/reducer';
import SearchWidgetReducer from '../../common/widgets/search-widget/reducer';
import Select2WidgetReducer from '../../common/widgets/select2-widget/reducer';

// Collections Reducer
import UserCollectionReducer from '../../backend/reducers/collections/UserCollectionReducer'
import BuildingCollectionReducer from '../../backend/reducers/collections/BuildingCollectionReducer';
import FloorCollectionReducer from '../../backend/reducers/collections/FloorCollectionReducer';
import FlatCollectionReducer from '../../backend/reducers/collections/FlatCollectionReducer';
import RoomCollectionReducer from '../../backend/reducers/collections/RoomCollectionReducer';

// Model Reducer
import UserModelReducer from '../../backend/reducers/models/UserModelReducer'
import BuildingModelReducer from '../../backend/reducers/models/BuildingModelReducer';
import FloorModelReducer from '../../backend/reducers/models/FloorModelReducer';
import FlatModelReducer from '../../backend/reducers/models/FlatModelReducer';
import RoomModelReducer from '../../backend/reducers/models/RoomModelReducer';
import LoginModelReducer from '../reducers/LoginModelReducer';


import { routerReducer } from 'react-router-redux';

const createReducer = () => {
    return combineReducers({
        User: combineReducers({
            collection: UserCollectionReducer,
            model: UserModelReducer,
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        Building: combineReducers({
            collection: BuildingCollectionReducer,
            model: BuildingModelReducer,
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        Floor: combineReducers({
            collection: FloorCollectionReducer,
            model: FloorModelReducer,
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        Flat: combineReducers({
            collection: FlatCollectionReducer,
            model: FlatModelReducer,
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        Room: combineReducers({
            collection: RoomCollectionReducer,
            model: RoomModelReducer,
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        Login: combineReducers({
            model: LoginModelReducer,
        }),
        common: combineReducers({
            formWidget: FormWidgetReducer,
            select2Widget: Select2WidgetReducer,
            snackbarWidget: SnackbarWidgetReducer,
            progressBarWidget: ProgressBarWidgetReducer,
            configData: ConfigDataReducer,
            systemError: SystemErrorReducer,
        }),
        routing: routerReducer,
    })
};

export default createReducer();
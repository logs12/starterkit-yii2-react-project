//========== /Constants react-redux-router app  ==========//
export * from './RoutesConstants';

//========== /Constants WIDGETS ==========//
export * from './WidgetConstants';

//========== /Constants backend ==========//
export * from '../../backend/constants/index';

//========== /Constants Аuthorization ==========//
export * from './LoginConstants';

export const CONFIG_DATA_GET = 'CONFIG_DATA_GET';

// Constants url request
export const CONFIG_DATA_URL_REQUEST = '/api-internal/config';


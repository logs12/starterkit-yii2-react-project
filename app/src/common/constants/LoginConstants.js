// Constants Аuthorization
export const LOGIN_FORM_WIDGET_REQUEST = 'LOGIN_FORM_WIDGET_REQUEST';
export const LOGIN_FORM_WIDGET_SUCCESS = 'LOGIN_FORM_WIDGET_SUCCESS';
export const LOGIN_FORM_WIDGET_ERROR = 'LOGIN_FORM_WIDGET_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export const LOGIN_MODEL_INIT = 'LOGIN_MODEL_INIT';
export const LOGIN_MODEL_UPDATE = 'LOGIN_MODEL_UPDATE';
export const LOGIN_FORM_MODEL_ERROR = 'LOGIN_FORM_MODEL_ERROR';

// Constants url request
export const LOGIN_URL_REQUEST = '/api-internal/login';
export const LOGOUT_URL_REQUEST = '/api-internal/logout';

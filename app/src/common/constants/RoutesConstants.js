//========== /Constants react-redux-router app ==========//
export const ADMIN_ROUTE = '/';
export const DASHBOARD_ROUTE = '/dashboard';
export const ADMINISTRATION_ROUTE = '/administration';

// user
export const USERS_ROUTE = '/administration/users';
export const USER_CREATE_ROUTE = '/administration/user/create';
export const USER_VIEW_ROUTE = id => `/administration/user/${id}`;
export const USER_UPDATE_ROUTE = id => `/administration/user/update/${id}`;
export const USER_DELETE_ROUTE = id => `/administration/user/delete/${id}`;


export const DIRECTORIES_ROUTE = '/directories';

// building
export const BUILDINGS_ROUTE = '/directories/buildings';
export const BUILDING_CREATE_ROUTE = '/directories/building/create';
export const BUILDING_VIEW_ROUTE = id => `/directories/building/${id}`;
export const BUILDING_UPDATE_ROUTE = id => `/directories/building/update/${id}`;
export const BUILDING_DELETE_ROUTE = id => `/directories/building/delete/${id}`;

// flat
export const FLATS_ROUTE = '/directories/flats';
export const FLAT_CREATE_ROUTE = '/directories/flat/create';
export const FLAT_VIEW_ROUTE = id => `/directories/flat/${id}`;
export const FLAT_UPDATE_ROUTE = id => `/directories/flat/update/${id}`;
export const FLAT_DELETE_ROUTE = id => `/directories/flat/delete/${id}`;

// room
export const ROOMS_ROUTE = '/directories/rooms';
export const ROOM_CREATE_ROUTE = '/directories/room/create';
export const ROOM_VIEW_ROUTE = id => `/directories/room/${id}`;
export const ROOM_UPDATE_ROUTE = id => `/directories/room/update/${id}`;
export const ROOM_DELETE_ROUTE = id => `/directories/room/delete/${id}`;

// floor
export const FLOORS_ROUTE = '/directories/floors';
export const FLOOR_CREATE_ROUTE = '/directories/floor/create';
export const FLOOR_VIEW_ROUTE = id => `/directories/floor/${id}`;
export const FLOOR_UPDATE_ROUTE = id => `/directories/floor/update/${id}`;
export const FLOOR_DELETE_ROUTE = id => `/directories/floor/delete/${id}`;

export const LOGIN_ROUTE = '/login';
export const ERROR_ROUTE = '/error';
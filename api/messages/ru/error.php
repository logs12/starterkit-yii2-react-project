<?php

// Перевод ошибок
return [
    'Your request was made with invalid credentials.' => 'Ошибка авторизации.',
    'Syntax error.' => 'Синтаксическая ошибка в передаваемых данных.',
    'Invalid JSON data in request body: Control character error, possibly incorrectly encoded.' => 'Неверные данные в формате JSON в теле запроса: Ошибка управляющего символа, возможно, неправильно закодирован.',
    'Illegal offset type' => 'Недопустимый тип',
    'Trying to get property of non-object' => 'Попытка получить несуществующее свойство объекта.',
];

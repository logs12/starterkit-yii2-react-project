<?php

return [
    'first_name' => 'Имя',
    'second_name' => 'Фамилия',
    'third_name' => 'Отчество',
    'phone' => 'Телефон',
    'email' => 'Почта',
    'password' => 'Пароль',
    'file_id' => 'Фотография',
    'status_id' => 'Статус',
];

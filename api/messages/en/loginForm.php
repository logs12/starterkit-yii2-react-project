<?php

return [
    'email' => 'E-mail address',
    'email_error' => 'Invalid e-mail address',
    'password' => 'Password',
    'password_error' => 'Invalid password',
];

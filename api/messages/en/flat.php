<?php

return [
    'id' => 'id',
    'floor_id' => 'identifier floor',
    'status_id' => 'identifier status',
    'created' => 'date created',
    'updated' => 'date updated',
    'deleted' => 'date deleted',
];
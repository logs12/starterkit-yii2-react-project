<?php

// Перевод ошибок
return [
    'Your request was made with invalid credentials.' => 'Your request was made with invalid credentials.',
    'Syntax error.' => 'Syntax error.',
    'Invalid JSON data in request body: Control character error, possibly incorrectly encoded.' => 'Invalid JSON data in request body: Control character error, possibly incorrectly encoded.',
    'Illegal offset type' => 'Недопустимый тип',
    'Trying to get property of non-object' => 'Trying to get property of non-object',
];

<?php

return [
    'id' => 'id',
    'number' => 'number floor',
    'building_id' => 'identifier building',
    'type_id' => 'identifier type',
    'status_id' => 'identifier status',
    'created' => 'date created',
    'updated' => 'date updated',
    'deleted' => 'date deleted',
];


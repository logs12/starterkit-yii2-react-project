<?php

return [
    'first_name' => 'First name',
    'second_name' => 'Second name',
    'third_name' => 'Third name',
    'phone' => 'Phone',
    'email' => 'Email',
    'password' => 'Password',
    'file_id' => 'Photo',
    'status_id' => 'status',
];

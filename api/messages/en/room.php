<?php

return [
    'id' => 'id',
    'trim' => 'Flag is there trim flat',
    'door' => 'Flag is there trim door',
    'door_knob' => 'Flag is there trim door knob',
    'crown_molding' => 'Flag is there crown molding',
    'size' => 'Size room',
    'flooring' => 'Flag is there flooring flat',
    'ceiling_height' => 'Ceiling height',
    'flat_id' => 'identifier flat',
    'status_id' => 'identifier status',
    'created' => 'date created',
    'updated' => 'date updated',
    'deleted' => 'date deleted',
];
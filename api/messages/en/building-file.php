<?php

return [
    'id' => 'Identifier record',
    'building_id' => 'Identifier building',
    'file_id' => 'Identifier file',
];
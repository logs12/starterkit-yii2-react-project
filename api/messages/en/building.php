<?php

return [
    'state' => 'State name',
    'city' => 'City name',
    'address_one' => 'One address',
    'address_two' => 'Two address',
    'floors' => 'Number of floors',
    'file_id' => 'Images building',
];
<?php

$params = require(__DIR__ . '/params.php');

// Слияние глобальных и локальных парамертов БД
$db = file_exists(__DIR__ . '/db.local.php')
    ? \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/db.php'), require(__DIR__ . '/db.local.php'))
    : require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        [
            'class' => 'app\components\Bootstrap',
        ],
        'log'
    ],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'authManager' => [
            //'class' => 'app\components\rbac\AuthManager',
            //'itemFile' => '@app/components/rbac/items.php', //Default path to items.php | NEW CONFIGURATIONS
            //'assignmentFile' => '@app/components/rbac/assignments.php', //Default path to assignments.php | NEW CONFIGURATIONS
            //'ruleFile' => '@app/components/rbac/rules.php', //Default path to rules.php | NEW CONFIGURATIONS
            'class' => 'yii\rbac\DbManager',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;

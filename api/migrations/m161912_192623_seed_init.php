<?php

use app\models\WebService;
use app\components\Migration;
use app\models\User;
use app\models\Status;
use app\models\AuthItem;
use app\models\Building;
use app\models\Floor;
use app\models\Flat;
use app\models\Room;

class m161912_192623_seed_init extends Migration
{
    public function init()
    {
        $this->operations = [
            [
                'up' => function () {
                    $this->execute('ALTER TABLE `user` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->execute('ALTER TABLE `file` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->execute('ALTER TABLE `status` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->execute('ALTER TABLE `building` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->execute('ALTER TABLE `floor` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->execute('ALTER TABLE `flat` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->execute('ALTER TABLE `room` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    for($i = 1; $i<= 90; $i++) {
                        // Пользователи
                        $authManager = Yii::$app->authManager;
                        $rootRole = $authManager->getRole(AuthItem::ROLE_ROOT);
                        $adminRole = $authManager->getRole(AuthItem::ROLE_ADMIN);
                        $userRole = $authManager->getRole(AuthItem::ROLE_USER);

                        if ($i === 1) {
                            $userName = $rootRole->description;
                        } elseif ($i === 2) {
                            $userName = $adminRole->description;
                        } elseif ($i === 3) {
                            $userName = $adminRole->description;
                        } else {
                            $userName = $userRole->description . ' #' . $i;
                        }

                        $phoneSuffix = str_pad($i, 4, "0", STR_PAD_LEFT);

                        $user = new User([
                            'first_name' => $userName,
                            'second_name' => 'Second name #' . $i,
                            'third_name' => 'Third name #' . $i,
                            'email' => 'user-'. $i .'@mail.ru',
                            'phone' => '+7908123' . $phoneSuffix,
                            'password' => '12345',
                        ]);
                        $user->saveOrError();

                        if ($i === 1) {
                            $authManager->assign($rootRole, $user->id);
                        } elseif ($i === 2) {
                            $authManager->assign($adminRole, $user->id);
                        } elseif ($i === 3) {
                            $authManager->assign($adminRole, $user->id);
                        } else {
                            $authManager->assign($userRole, $user->id);
                        }

                        $building = new Building([
                            'state' => 'AR #' . $i,
                            'city' => 'Kansas #' . $i ,
                            'address_one' => '90, McHolland Drive #' . $i,
                            'address_two' => '17 Jonnesway road #' . $i,
                            'status_id' => 1
                        ]);

                        $building->saveOrError();

                        // Generating demo floor
                        for ($j = 0; $j <= 2; $j++) {
                            $floor = new Floor([
                                'number' => $j,
                                'title' => 'floor #' . $j,
                                'building_id' => $building->id,
                                'status_id' => 1,
                            ]);
                            $floor->saveOrError();

                            // Generating demo flat
                            for ($t = 0; $t <= 50; $t++) {

                                $flat = new Flat([
                                    'floor_id' => $floor->id,
                                    'number' => $t,
                                    'title' => 'title #' . $t,
                                    'status_id' => 1,
                                ]);

                                $flat->saveOrError();


                                $room = new Room([
                                    'flat_id' => $flat->id,
                                    'type_id' => 1,
                                    'size' => 45,
                                    'flooring' => 'wood',
                                    'ceiling_height' => 20,
                                ]);

                                $room->saveOrError();

                                $room = new Room([
                                    'flat_id' => $flat->id,
                                    'type_id' => 2,
                                    'size' => 22,
                                    'flooring' => 'wood',
                                    'ceiling_height' => 20,
                                ]);

                                $room->saveOrError();
                            }
                        }
                    }
                },
                'down' => function () {
                    User::deleteAll();
                    Building::deleteAll();

                    Yii::$app->authManager->removeAllAssignments();
                }
            ]
        ];
    }
}

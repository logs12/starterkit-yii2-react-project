<?php

use app\components\Migration;
use yii\db\Schema;
use app\models\Status;
use app\models\Type;

class m161025_194027_init extends Migration
{
    public function init()
    {
        $this->operations = [

            [
                'up' => function () {
                    $this->createTable('{{%status}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'title' => $this->string(255)->notNull()->comment('Название'),
                        'name' => $this->string(255)->notNull()->comment('Системное название'),
                        'UNIQUE KEY `status-name--unique` (`name`)',
                    ], $this->getTableOptions('Все статусы системы'));
                },
                'down' => function () {
                    $this->dropTable('status');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%file}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'name' => $this->string(255)->notNull()->comment('Название файла в нашей файловой системе'),
                        'filename' => $this->string(255)->notNull()->comment('Оригинальное название файла'),
                        'extension' => $this->string(10)->notNull()->comment('Расширение файла'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор статуса'),
                        'created' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP COMMENT "Дата добавления записи"',
                        'updated' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата изменения записи"',
                        'deleted' => Schema::TYPE_TIMESTAMP . ' NULL COMMENT "Дата удаления записи"',
                        'created_by' => $this->integer()->unsigned()->comment('Идентификатор пользователя создавшего запись'),
                        'updated_by' => $this->integer()->unsigned()->comment('Идентификатор пользователя редактировавшего запись'),
                        'CONSTRAINT file_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Файлы'));
                },
                'down' => function () {
                    $this->dropTable('file');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->createTable('{{%user}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'first_name' => $this->string(150)->notNull()->comment('Имя'),
                        'second_name' => $this->string(150)->comment('Фамилия'),
                        'third_name' => $this->string(150)->comment('Отчество'),
                        'email' => $this->string(255)->notNull()->comment('Электропочта'),
                        'file_id' => $this->integer()->unsigned()->comment('Идентификатор файла фотографии'),
                        'phone' => $this->string(25)->comment('Телефон'),
                        'password_hash' => $this->string(60)->notNull()->comment('Хэш пароля'),
                        'auth_key' => $this->string(32)->comment('Ключ подтверждения для cookie аутентификации'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор статуса'),
                        //'role_name' => $this->string(64)->comment('Наименование роли'),
                        'created' => $this->integer(11)->comment('Дата добавления записи'),
                        'updated' => $this->integer(11)->comment('Дата изменения записи'),
                        'deleted' => $this->integer(11)->comment('Дата удаления записи'),
                        'CONSTRAINT user_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT user_2_file FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Users'));
                },
                'down' => function () {
                    $this->dropTable('{{%user}}');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->createTable('{{%type}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier'),
                        'title' => $this->string(255)->notNull()->comment('Type name'),
                        'name' => $this->string(255)->notNull()->comment('System name'),
                        'UNIQUE KEY `type-name--unique` (`name`)',
                    ], $this->getTableOptions('All type system'));
                },
                'down' => function () {
                    $this->dropTable('type');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->createTable('{{%building}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier'),
                        'state' => $this->string(255)->notNull()->comment('State name'),
                        'city' => $this->string(255)->notNull()->comment('City name'),
                        'address_one' => $this->string(255)->notNull()->comment('One address'),
                        'address_two' => $this->string(255)->notNull()->comment('Two address'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Identifier status'),
                        'created' => $this->integer(11)->comment('Date created'),
                        'updated' => $this->integer(11)->comment('Date updated'),
                        'deleted' => $this->integer(11)->comment('Date deleted'),
                        'CONSTRAINT building_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('building'));
                },
                'down' => function () {
                    $this->dropTable('{{%building}}');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->createTable('{{%building_file}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'building_id' => $this->integer()->unsigned()->comment('Идентификатор здания'),
                        'file_id' => $this->integer()->unsigned()->comment('Идентификатор файла'),
                        'CONSTRAINT building_file_2_building FOREIGN KEY (building_id) REFERENCES building (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT building_file_2_file_id FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('building images'));
                },
                'down' => function () {
                    $this->dropTable('{{%building_file}}');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->createTable('{{%floor}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier'),
                        'number' => $this->integer()->unsigned()->notNull()->comment('Number floor'),
                        'title' => $this->string(155)->notNull()->comment('Title floor'),
                        'building_id' => $this->integer()->unsigned()->comment('Identifier building'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Identifier status'),
                        'created' => $this->integer(11)->comment('Date created'),
                        'updated' => $this->integer(11)->comment('Date updated'),
                        'deleted' => $this->integer(11)->comment('Date deleted'),
                        'CONSTRAINT floor_2_building FOREIGN KEY (building_id) REFERENCES building (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT floor_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('floor'));
                },
                'down' => function () {
                    $this->dropTable('{{%floor}}');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->createTable('{{%floor_file}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier record'),
                        'floor_id' => $this->integer()->unsigned()->comment('Identifier floor'),
                        'file_id' => $this->integer()->unsigned()->comment('Identifier file'),
                        'CONSTRAINT floor_file_2_floor FOREIGN KEY (floor_id) REFERENCES floor (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT floor_file_2_file_id FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Floor images'));
                },
                'down' => function () {
                    $this->dropTable('{{%floor_file}}');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->createTable('{{%flat}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier'),
                        'number' => $this->integer()->unsigned()->notNull()->comment('Number flat'),
                        'title' => $this->string(155)->comment('Title flat'),
                        'floor_id' => $this->integer()->unsigned()->comment('Identifier floor'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Identifier status'),
                        'created' => $this->integer(11)->comment('Date created'),
                        'updated' => $this->integer(11)->comment('Date updated'),
                        'deleted' => $this->integer(11)->comment('Date deleted'),
                        'CONSTRAINT flat_2_floor FOREIGN KEY (floor_id) REFERENCES floor (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT flat_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('flat'));
                },
                'down' => function () {
                    $this->dropTable('{{%flat}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%flat_file}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier record'),
                        'flat_id' => $this->integer()->unsigned()->comment('Identifier flat'),
                        'file_id' => $this->integer()->unsigned()->comment('Identifier file'),
                        'CONSTRAINT flat_file_2_flat FOREIGN KEY (flat_id) REFERENCES flat (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT flat_file_2_file_id FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Floor images'));
                },
                'down' => function () {
                    $this->dropTable('{{%flat_file}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%room}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier'),
                        'trim' => $this->boolean()->notNull()->defaultValue(0)->comment('Flag is there trim flat'),
                        'door' => $this->boolean()->notNull()->defaultValue(0)->comment('Flag is there trim door'),
                        'door_knob' => $this->boolean()->notNull()->defaultValue(0)->comment('Flag is there trim door knob'),
                        'crown_molding' => $this->boolean()->notNull()->defaultValue(0)->comment('Flag is there crown molding'),
                        'size' => $this->double()->comment('Size room'),
                        'flooring' => $this->string(155)->notNull()->comment('Flooring flat'),
                        'ceiling_height' => $this->double()->comment('Ceiling height'),
                        'flat_id' => $this->integer()->unsigned()->comment('Identifier flat'),
                        'type_id' => $this->integer()->unsigned()->comment('Identifier type'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Identifier status'),
                        'created' => $this->integer(11)->comment('Date created'),
                        'updated' => $this->integer(11)->comment('Date updated'),
                        'deleted' => $this->integer(11)->comment('Date deleted'),
                        'CONSTRAINT room_2_flat FOREIGN KEY (flat_id) REFERENCES flat (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT room_2_type FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT room_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('room'));
                },
                'down' => function () {
                    $this->dropTable('{{%room}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%room_file}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier record'),
                        'room_id' => $this->integer()->unsigned()->comment('Identifier room'),
                        'file_id' => $this->integer()->unsigned()->comment('Identifier file'),
                        'CONSTRAINT room_file_2_room FOREIGN KEY (room_id) REFERENCES room (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT room_file_2_file_id FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Floor images'));
                },
                'down' => function () {
                    $this->dropTable('{{%room_file}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {

                    // Инициализация статусов
                    $status = new Status([
                        'title' => 'Активный',
                        'name' => 'active',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Неактивный',
                        'name' => 'inactive',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Удалён',
                        'name' => 'deleted',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Не применён',
                        'name' => 'not_applied',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Применён',
                        'name' => 'applied',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Rented',
                        'name' => 'rented',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Vacant',
                        'name' => 'vacant',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Under',
                        'name' => 'under',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Construction',
                        'name' => 'construction',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Rent',
                        'name' => 'rent',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Ready',
                        'name' => 'ready',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Tenant gave notice',
                        'name' => 'tenant_gave_notice',
                    ]);
                    $status->saveOrError();

                    $type = new Type([
                        'title' => 'Kitchen',
                        'name' => 'kitchen',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Bedroom',
                        'name' => 'bedroom',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Master',
                        'name' => 'master',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Toilet',
                        'name' => 'toilet',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Bathroom',
                        'name' => 'bathroom',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Living',
                        'name' => 'living',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Laundry',
                        'name' => 'laundry',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Basement',
                        'name' => 'basement',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Hallway',
                        'name' => 'hallway',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Dining',
                        'name' => 'dining',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Office',
                        'name' => 'office',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Study',
                        'name' => 'study',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Comms',
                        'name' => 'comms',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Storage',
                        'name' => 'storage',
                    ]);
                    $type->saveOrError();
                },
                'down' => function () {
                    Status::deleteAll();
                    Type::deleteAll();
                },
                'transactional' => true,
            ],
        ];
    }
}

<?php

namespace app\models;

use app\components\behaviors\UploadFileBehavior;
use app\components\services\FileService;
use Yii;
use yii\helpers\ArrayHelper;
use app\components\BaseActiveRecord;

/**
 * This is the model class for table "{{%floor_file}}".
 *
 * @property integer $id
 * @property integer $floor_id
 * @property integer $file_id
 */
class FloorFile extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%floor_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['floor_id', 'integer'],
            ['file_id', 'integer'],
            ['floor_id', 'exist', 'skipOnError' => true, 'targetClass' => Floor::className(), 'targetAttribute' =>
            ['floor_id' => 'id']],
            ['file_id', 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app/floor-file', 'id'),
            'floor_id' => \Yii::t('app/floor-file', 'floor_id'),
            'file_id' => \Yii::t('app/floor-file', 'file_id'),
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'floor_id' => function ($model) {
                return $this->getIntOrNull($model->floor_id);
            },
            'file_id' => function ($model) {
                return $this->getIntOrNull($model->file_id);
            },
        ]);
    }

    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
}

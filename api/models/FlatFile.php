<?php

namespace app\models;

use app\components\behaviors\UploadFileBehavior;
use app\components\services\FileService;
use Yii;
use yii\helpers\ArrayHelper;
use app\components\BaseActiveRecord;

/**
 * This is the model class for table "{{%flat_file}}".
 *
 * @property integer $id
 * @property integer $flat_id
 * @property integer $file_id
 */
class FlatFile extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%flat_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['flat_id', 'integer'],
            ['flat_id', 'exist', 'skipOnError' => true, 'targetClass' => Flat::className(), 'targetAttribute' => ['flat_id' => 'id']],

            ['file_id', 'integer'],
            ['file_id', 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app/flat-file', 'id'),
            'flat_id' => \Yii::t('app/flat-file', 'flat_id'),
            'file_id' => \Yii::t('app/flat-file', 'file_id'),
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'flat_id' => function ($model) {
                return $this->getIntOrNull($model->flat_id);
            },
            'file_id' => function ($model) {
                return $this->getIntOrNull($model->file_id);
            },
        ]);
    }

    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
}

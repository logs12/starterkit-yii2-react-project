<?php

namespace app\models;

use app\components\BaseActiveRecord;
use app\components\behaviors\UploadFileBehavior;
use app\components\services\CacheService;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "building".
 *
 * @property string $id
 * @property string $state
 * @property string $city
 * @property string $address_one
 * @property string $address_two
 * @property integer $file_id
 *
 *
 * @property Status $status
 * @property BuildingFile[] $buildingFiles
 */
class Building extends BaseActiveRecord
{

    public $photos;

    public static function tableName()
    {
        return 'building';
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'uploadFile' => [
                'class' => UploadFileBehavior::className(),
                'relationModelAttributes' => ['photos' => [BuildingFile::className() => ['file_id', 'building_id']]],
            ]
        ]);
    }

    public function rules()
    {
        return [

            ['state', 'required'],
            ['state', 'string', 'max' => 255,],
            ['state', 'string', 'max' => 255],


            ['city', 'required'],
            ['city', 'string', 'max' => 255,],
            ['city', 'string', 'max' => 255],


            ['address_one', 'required'],
            ['address_one', 'string', 'max' => 255,],
            ['address_one', 'string', 'max' => 255],


            ['address_two', 'required'],
            ['address_two', 'string', 'max' => 255,],
            ['address_two', 'string', 'max' => 255],

            ['photos', 'file', 'maxFiles' => 50, 'extensions' => 'png, jpg'],

            ['status_id', 'integer'],
            ['status_id', 'default', 'value' => CacheService::getStatusByName(static::STATUS_ACTIVE)->id],
            ['status_id', 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), ['id' => function($model) {
            return static::getIntOrNull($model->id);
        },
            'state',
            'city',
            'address_one',
            'address_two',
            'floors',
            'photos' => function($model) {
                return $model->buildingFiles;
            },
            'address_full' => function($model) {
                return $model->address_one.' '.$model->address_two;
            }
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'state' => \Yii::t('app/building', 'state'),
            'city' => \Yii::t('app/building', 'city'),
            'address_one' => \Yii::t('app/building', 'address_one'),
            'address_two' => \Yii::t('app/building', 'address_two'),
            'floors' => \Yii::t('app/building', 'floors'),
            'file_id' => \Yii::t('app/building', 'file_id'),
        ];
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function getBuildingFiles()
    {
        return $this->hasMany(File::className(), ['id' => 'file_id'])
            ->viaTable('building_file', ['building_id' => 'id']);
    }

    public function getFloors()
    {
        return $this->hasMany(Floor::className(), ['building_id' => 'id']);
    }
}

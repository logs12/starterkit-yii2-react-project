<?php

namespace app\models;

use app\components\BaseActiveRecord;
use app\components\services\CacheService;
use app\components\services\FileService;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "file".
 *
 * @property string $id
 * @property string $name
 * @property string $filename
 * @property string $extension
 * @property string $status_id
 * @property string $created
 * @property string $updated
 * @property string $deleted
 * @property integer $created_by
 * @property integer $updated_by
 *
 *
 * @property BuildingFile[] $buildingFiles
 * @property FloorFile[] $floorFiles
 * @property FlatFile[] $flatFiles
 * @property RoomFile[] $roomFiles
 */
class File extends BaseActiveRecord
{
    public function init()
    {
        parent::init();

        $this->on(self::AFTER_SOFT_DELETE, function() {

            // При удалении файла, удаляем связи из таблицы {{%building_file}}
            $buildingFiles = $this->buildingFiles;
            foreach ($buildingFiles as $buildingFile) {
                $buildingFile->delete();
            }

            // При удалении файла, удаляем связи из таблицы {{%floor_file}}
            $floorFiles = $this->floorFiles;
            foreach ($floorFiles as $floorFile) {
                $floorFile->delete();
            }
            
            // При удалении файла, удаляем связи из таблицы {{%flat_file}}
            $flatFiles = $this->flatFiles;
            foreach ($flatFiles as $flatFile) {
                $flatFile->delete();
            }
            
            // При удалении файла, удаляем связи из таблицы {{%room_file}}
            $roomFiles = $this->roomFiles;
            foreach ($roomFiles as $roomFile) {
                $roomFile->delete();
            }

        });
    }

    public static function tableName()
    {
        return 'file';
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'name',
            'extension',
            'filename',
            'path' => function() {
                return $this->filePath;
            }
        ]);
    }

    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'string', 'max' => 255],

            ['filename', 'trim'],
            ['filename', 'string', 'max' => 255],

            ['extension', 'trim'],
            ['extension', 'string', 'max' => 10],

            ['status_id', 'integer'],
            ['status_id', 'default', 'value' => CacheService::getStatusByName(static::STATUS_ACTIVE)->id],
            ['status_id', 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    public function getBuildingFiles()
    {
        return $this->hasMany(BuildingFile::className(), ['file_id' => 'id']);
    }

    public function getFloorFiles()
    {
        return $this->hasMany(FloorFile::className(), ['file_id' => 'id']);
    }

    public function getFlatFiles()
    {
        return $this->hasMany(FlatFile::className(), ['file_id' => 'id']);
    }

    public function getRoomFiles()
    {
        return $this->hasMany(RoomFile::className(), ['file_id' => 'id']);
    }

    public function getFilePath()
    {
        return FileService::getFilePath($this);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
}

<?php

namespace app\models;

use app\components\behaviors\UploadFileBehavior;
use app\components\services\FileService;
use Yii;
use yii\helpers\ArrayHelper;
use app\components\BaseActiveRecord;

/**
 * This is the model class for table "{{%room_file}}".
 *
 * @property integer $id
 * @property integer $room_id
 * @property integer $file_id
 */
class RoomFile extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%room_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['room_id', 'integer'],
            ['file_id', 'integer'],
            ['room_id', 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' =>
            ['room_id' => 'id']],
            ['file_id', 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app/room-file', 'id'),
            'room_id' => \Yii::t('app/room-file', 'room_id'),
            'file_id' => \Yii::t('app/room-file', 'file_id'),
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'room_id' => function ($model) {
                return $this->getIntOrNull($model->room_id);
            },
            'file_id' => function ($model) {
                return $this->getIntOrNull($model->file_id);
            },
        ]);
    }

    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
}

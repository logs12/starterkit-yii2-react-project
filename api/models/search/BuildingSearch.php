<?php
namespace app\models\search;

use app\components\queries\BaseQuery;
use app\models\Building as BuildingModel;
use yii\helpers\ArrayHelper;

class BuildingSearch extends BuildingModel
{
    public function search($params)
    {
        /*$query = static::find();

        $query->handleParam($params, 'id', BaseQuery::CONDITION_AND);

        $query->orFilterWhere(['like', static::field('state'), ArrayHelper::getValue($params, 'condition.state')]);
        $query->orFilterWhere(['like', static::field('city'), ArrayHelper::getValue($params, 'condition.city')]);
        $query->orFilterWhere(['like', static::field('address_one'), ArrayHelper::getValue($params, 'condition.address_one')]);
        $query->orFilterWhere(['like', static::field('address_two'), ArrayHelper::getValue($params, 'condition.address_two')]);

        if (($order = ArrayHelper::getValue($params, 'order')) && is_array($order)) {
            $query = static::queryOrder($order, $query);
        }

        if (($id = ArrayHelper::getValue($params, 'condition.id')) && is_array($id) && isset($id[1])) {
            $query->andWhere([$id[0], static::field('id'), $id[1]]);
        }

        if (($order = ArrayHelper::getValue($params, 'order')) && is_array($order)) {
            $query = static::queryOrder($order, $query);
        } else {
            $query->orderBy([BuildingModel::field('city') => SORT_ASC]);
        }

        return $query;*/

        $query = static::find();

        if ($firstName = ArrayHelper::getValue($params, 'condition.state')) {
            $query->where(['like', static::field('state'), $firstName]);
        }

        if ($secondName = ArrayHelper::getValue($params, 'condition.city')) {
            $query->orWhere(['like', static::field('city'), $secondName]);
        }

        if ($thirdName = ArrayHelper::getValue($params, 'condition.address_one')) {
            $query->orWhere(['like', static::field('address_one'), $thirdName]);
        }

        if ($thirdName = ArrayHelper::getValue($params, 'condition.address_two')) {
            $query->orWhere(['like', static::field('address_two'), $thirdName]);
        }

        if (($order = ArrayHelper::getValue($params, 'order')) && is_array($order)) {
            $query = static::queryOrder($order, $query);
        }

        return $query;
    }
}
<?php
namespace app\models\search;

use app\components\queries\BaseQuery;
use app\models\Floor as FloorModel;
use yii\helpers\ArrayHelper;

class FloorSearch extends FloorModel
{
    public function search($params)
    {

        $query = static::find();

        if ($firstName = ArrayHelper::getValue($params, 'condition.title')) {
            $query->where(['like', static::field('title'), $firstName]);
        }
        if (($order = ArrayHelper::getValue($params, 'order')) && is_array($order)) {
            $query = static::queryOrder($order, $query);
        }

        return $query;
    }
}
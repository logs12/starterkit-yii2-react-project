<?php

namespace app\models;
use app\components\BaseActiveRecord;

use app\components\behaviors\UploadFileBehavior;
use app\components\services\CacheService;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "floor".
 *
 * @property int $id
 * @property int $number
 * @property int $building_id
 * @property int $status_id
 * @property int $created
 * @property int $updated
 * @property int $deleted
 *
 * @property Building $building
 * @property Flat[] $flats
 * @property Status $status
 * @property FloorFile[] $floorFiles
 */
class Floor extends BaseActiveRecord
{
    public $photos;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%floor}}';
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'uploadFile' => [
                'class' => UploadFileBehavior::className(),
                'relationModelAttributes' => ['photos' => [FloorFile::className() => ['file_id', 'floor_id']]],
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['number'], 'integer'],

            ['title', 'string', 'max' => 255],

            ['building_id', 'integer'],
            ['building_id', 'exist', 'skipOnError' => true, 'targetClass' => Building::className(), 'targetAttribute' => ['building_id' => 'id']],

            ['photos', 'file', 'maxFiles' => 50, 'extensions' => 'png, jpg'],

            ['status_id', 'integer'],
            ['status_id', 'default', 'value' => CacheService::getStatusByName(static::STATUS_ACTIVE)->id],
            ['status_id', 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/floor', 'id'),
            'number' => Yii::t('app/floor', 'number'),
            'building_id' => Yii::t('app/floor', 'building_id'),
            'status_id' => Yii::t('app/floor', 'status_id'),
            'created' => Yii::t('app/floor', 'created'),
            'updated' => Yii::t('app/floor', 'updated'),
            'deleted' => Yii::t('app/floor', 'deleted'),
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'id' => function($model) {
                return static::getIntOrNull($model->id);
            },
            'number',
            'title',
            'photos' => function($model) {
                return $model->floorFiles;
            },
            //'flats',
            'building_id',
            'building_address' => function ($model) {
                if ($building = $model->building) {
                    return $building->address_one.' '.$building->address_two;
                }
                return null;
            },
        ]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Building::className(), ['id' => 'building_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
/*    public function getFlats()
    {
        return $this->hasMany(Flat::className(), ['floor_id' => 'id']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function getFloorFiles()
    {
        return $this->hasMany(File::className(), ['id' => 'file_id'])
            ->viaTable('floor_file', ['floor_id' => 'id']);
    }
}

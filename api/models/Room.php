<?php

namespace app\models;
use app\components\BaseActiveRecord;

use app\components\behaviors\UploadFileBehavior;
use app\components\services\CacheService;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "room".
 *
 * @property int $id
 * @property bool $trim
 * @property bool $door
 * @property bool $door_knob
 * @property bool $crown_molding
 * @property double $size
 * @property string $flooring
 * @property double $ceiling_height
 * @property int $flat_id
 * @property int $type_id
 * @property int $status_id
 * @property int $created
 * @property int $updated
 * @property int $deleted
 *
 * @property Flat $flat
 * @property Type $type
 * @property Status $status
 * @property RoomFile[] $roomFiles
 */
class Room extends BaseActiveRecord
{

    public $photos;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%room}}';
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'uploadFile' => [
                'class' => UploadFileBehavior::className(),
                'relationModelAttributes' => ['photos' => [RoomFile::className() => ['file_id', 'room_id']]],
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created', 'updated', 'deleted'], 'integer'],

            ['trim', 'boolean'],
            ['door', 'boolean'],
            ['door_knob', 'boolean'],
            ['crown_molding', 'boolean'],
            ['size', 'double'],
            ['flooring', 'string'],
            ['ceiling_height', 'double'],

            ['flat_id', 'integer'],
            ['flat_id', 'exist', 'skipOnError' => true, 'targetClass' => Flat::className(), 'targetAttribute' =>
            ['flat_id' => 'id']],

            ['type_id', 'integer'],
            ['type_id', 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type_id' => 'id']],

            ['photos', 'file', 'maxFiles' => 50, 'extensions' => 'png, jpg'],

            ['status_id', 'integer'],
            ['status_id', 'default', 'value' => CacheService::getStatusByName(static::STATUS_ACTIVE)->id],
            ['status_id', 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/room', 'id'),
            'trim' => Yii::t('app/room', 'trim'),
            'door' => Yii::t('app/room', 'door'),
            'door_knob' => Yii::t('app/room', 'door_knob'),
            'crown_molding' => Yii::t('app/room', 'crown_molding'),
            'size' => Yii::t('app/room', 'size'),
            'flooring' => Yii::t('app/room', 'flooring'),
            'ceiling_height' => Yii::t('app/room', 'ceiling_height'),
            'flat_id' => Yii::t('app/room', 'flat_id'),
            'status_id' => Yii::t('app/room', 'status_id'),
            'created' => Yii::t('app/room', 'created'),
            'updated' => Yii::t('app/room', 'updated'),
            'deleted' => Yii::t('app/room', 'deleted'),
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'id' => function($model) {
                return static::getIntOrNull($model->id);
            },
            'trim',
            'door',
            'door_knob',
            'crown_molding',
            'size',
            'flooring',
            'ceiling_height',
            'number_flat' => function($model) {
                if ($flat = $model->flat) {
                    return $flat->number;
                }
                return null;
            },
            'type' => function($model) {
                if ($type = $model->type) {
                    return $type->title;
                }
            },
            'photos' => function($model) {
                return $model->roomFiles;
            }
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlat()
    {
        return $this->hasOne(Flat::className(), ['id' => 'flat_id']);
    }

    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }

    public function getRoomFiles()
    {
        return $this->hasMany(File::className(),['id' => 'file_id'])
            ->viaTable('flat_file', ['flat_id' => 'id']);
    }
}

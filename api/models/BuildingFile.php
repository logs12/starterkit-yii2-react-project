<?php

namespace app\models;

use app\components\behaviors\UploadFileBehavior;
use app\components\services\FileService;
use Yii;
use yii\helpers\ArrayHelper;
use app\components\BaseActiveRecord;

/**
 * This is the model class for table "{{%building_file}}".
 *
 * @property integer $id
 * @property integer $building_id
 * @property integer $file_id
 */
class BuildingFile extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%building_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['building_id', 'integer'],
            ['building_id', 'exist', 'skipOnError' => true, 'targetClass' => Building::className(), 'targetAttribute' => ['building_id' => 'id']],
            
            ['file_id', 'integer'],
            ['file_id', 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app/building-file', 'id'),
            'building_id' => \Yii::t('app/building-file', 'building_id'),
            'file_id' => \Yii::t('app/building-file', 'file_id'),
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'building_id' => function ($model) {
                return $this->getIntOrNull($model->building_id);
            },
            'file_id' => function ($model) {
                return $this->getIntOrNull($model->file_id);
            },
        ]);
    }

    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
}

<?php

namespace app\models;
use app\components\BaseActiveRecord;

use app\components\behaviors\UploadFileBehavior;
use app\components\services\CacheService;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "flat".
 *
 * @property int $id
 * @property int $number
 * @property string $title
 * @property int $floor_id
 * @property int $status_id
 * @property int $created
 * @property int $updated
 * @property int $deleted
 *
 * @property Floor $floor
 * @property Status $status
 * @property Room[] $rooms
 * @property FlatFile[] $flatFiles
 */
class Flat extends BaseActiveRecord
{

    public $photos;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%flat}}';
    }


    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'uploadFile' => [
                'class' => UploadFileBehavior::className(),
                'relationModelAttributes' => ['photos' => [FlatFile::className() => ['file_id', 'flat_id']]],
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['number'], 'required'],
            [['number'], 'integer'],

            ['title', 'string', 'max' => 255],

            [['floor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Floor::className(), 'targetAttribute' =>
            ['floor_id' => 'id']],

            ['photos', 'file', 'maxFiles' => 50, 'extensions' => 'png, jpg'],

            ['status_id', 'integer'],
            ['status_id', 'default', 'value' => CacheService::getStatusByName(static::STATUS_ACTIVE)->id],
            ['status_id', 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/flat', 'id'),
            'floor_id' => Yii::t('app/flat', 'floor_id'),
            'status_id' => Yii::t('app/flat', 'status_id'),
            'created' => Yii::t('app/flat', 'created'),
            'updated' => Yii::t('app/flat', 'updated'),
            'deleted' => Yii::t('app/flat', 'deleted'),
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'id' => function($model) {
                return static::getIntOrNull($model->id);
            },
            'number',
            'title',
            'number_floor' => function($model) {
                if ($floor = $model->floor) {
                    return $floor->number;
                }
                return null;
            },
            'building_id' => function() {

            },
            'floor_id' => function($model) {
                if ($floor = $model->floor) {
                    return $floor->id;
                }
                return null;
            },
            'floor_title' => function($model) {
                if ($floor = $model->floor) {
                    return $floor->title;
                }
                return null;
            },
            'photos' => function($model) {
                return $model->flatFiles;
            }
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFloor()
    {
        return $this->hasOne(Floor::className(), ['id' => 'floor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['flat_id' => 'id']);
    }

    public function getFlatFiles()
    {
        return $this->hasMany(File::className(), ['id' => 'file_id'])
            ->viaTable('flat_file', ['flat_id' => 'id']);
    }
}

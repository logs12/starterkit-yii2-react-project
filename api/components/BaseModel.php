<?php

namespace app\components;

use yii\base\Model;

class BaseModel extends Model
{
    use BaseModelTrait;
}
<?php

namespace app\components\validators;

use Yii;
use yii\validators\Validator;

class MoneyValidator extends Validator
{
    public $message = '{attribute} must be a number.';

    public $moneyPattern = '/^\s*[-+]?[0-9]*(\.|,)?[0-9]+([eE][-+]?[0-9]+)?\s*$/';

    public function validateAttribute($model, $attribute)
    {
        try {
            $value = preg_replace('/\s+/', '', $model->{$attribute});
            if (!preg_match($this->moneyPattern, $value)) {
                $model->{$attribute} = $model->getOldAttribute($attribute); // Дабы обезопасить...
                $this->addError($model, $attribute, Yii::t('yii', $this->message), $params = []);
            } else {
                // Преобразуем русские MONEY
                $model->{$attribute} = (float) preg_replace('/,/', '.', $value);
            }

        } catch (\ErrorException $er) {
            $model->{$attribute} = $model->getOldAttribute($attribute);; // Дабы обезопасить...
            $this->addError($model, $attribute, Yii::t('yii', $this->message), $params = []);
        }
    }
}
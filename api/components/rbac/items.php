<?php
return [
    'ROLE_ROOT' => [
        'type' => 1,
        'description' => 'ROLE_ROOT',
    ],
    'ROLE_ADMIN' => [
        'type' => 1,
        'description' => 'ROLE_ADMIN',
        'children' => [
            'ROLE_ROOT',
        ],
    ],
    'PERMISSION_USER_GET' => [
        'type' => 2,
        'description' => 'PERMISSION_USER_GET',
    ],
    'PERMISSION_USER_CREATE' => [
        'type' => 2,
        'description' => 'PERMISSION_USER_CREATE',
    ],
    'PERMISSION_USER_UPDATE' => [
        'type' => 2,
        'description' => 'PERMISSION_USER_UPDATE',
    ],
    'PERMISSION_USER_DELETE' => [
        'type' => 2,
        'description' => 'PERMISSION_USER_DELETE',
    ],
];

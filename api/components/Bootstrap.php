<?php
namespace app\components;

use yii;
use yii\base\BootstrapInterface;
use yii\mail\BaseMailer;
use yii\validators\Validator;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        // Переопределение базового валидатора exist ввиду
        // того, что там не ставится таблица у полей (table.field <> table.field)
        Validator::$builtInValidators['exist'] = 'app\components\validators\ExistValidator';
        Validator::$builtInValidators['unique'] = 'app\components\validators\UniqueValidator';
        Validator::$builtInValidators['money'] = 'app\components\validators\MoneyValidator';
    }
}
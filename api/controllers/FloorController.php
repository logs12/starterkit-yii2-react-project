<?php

namespace app\controllers;

use app\components\controllers\BaseActiveController;
use app\models\AuthItem;
use yii\base\UserException;

class FloorController extends BaseActiveController
{
    public $modelClass = 'app\models\Floor';

    public function getQuery()
    {
        $query = parent::getQuery();
        $query->with(['status']);

        return $query;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case self::ACTION_INDEX:
                $this->checkPermissions([AuthItem::PERMISSION_FLOOR_GET]);
                break;
            case self::ACTION_VIEW:
                $this->checkPermissions([
                    AuthItem::PERMISSION_FLOOR_GET,
                ]);
                break;
            case self::ACTION_UPDATE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_FLOOR_GET,
                    AuthItem::PERMISSION_FLOOR_UPDATE,
                ]);
                break;
            case self::ACTION_CREATE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_FLOOR_GET,
                    AuthItem::PERMISSION_FLOOR_CREATE,
                ]);
                break;
            case self::ACTION_DELETE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_FLOOR_GET,
                    AuthItem::PERMISSION_FLOOR_DELETE,
                ]);
                break;
            default:
                throw new UserException('Не найдено разрешение для действия:' . $action . ' в контроллере ' . get_class($this));
        }
    }
}
